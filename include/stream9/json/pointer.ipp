#ifndef STREAM9_JSON_POINTER_IPP
#define STREAM9_JSON_POINTER_IPP

#include "pointer.hpp"

namespace stream9::json {

/*
 * definition
 */
inline pointer::pointer() = default;

inline pointer::const_iterator pointer::
begin() const
{
    return m_path.begin();
}

inline pointer::const_iterator pointer::
end() const
{
    return m_path.end();
}

inline pointer::size_type pointer::
size() const
{
    return m_path.size();
}

void pointer::
push_back(pointer_::object_key auto const& key)
{
    m_path.emplace_back(key);
}

inline void pointer::
push_back(array_index_type const index)
{
    m_path.emplace_back(std::to_string(index.value()));
}

inline void pointer::
pop_back()
{
    m_path.pop_back();
}

inline pointer::iterator pointer::
insert(const_iterator const pos, pointer_::object_key auto const& key)
{
    return m_path.emplace(pos, key);
}

inline pointer::iterator pointer::
insert(const_iterator const pos, array_index_type const index)
{
    return m_path.insert(pos, std::to_string(index.value()));
}

inline pointer::iterator pointer::
erase(const_iterator const pos)
{
    return m_path.erase(pos);
}

inline pointer::iterator pointer::
erase(const_iterator const first, const_iterator const end)
{
    return m_path.erase(first, end);
}

inline void pointer::
clear()
{
    m_path.clear();
}

inline pointer::
operator std::string() const
{
    return to_string();
}

inline pointer::
operator json::value() const
{
    return to_value();
}

inline bool pointer::
operator==(std::same_as<pointer> auto const& rhs) const
{
    return m_path == rhs.m_path;
}

inline auto pointer::
operator<=>(std::same_as<pointer> auto const& rhs) const
{
    return m_path <=> rhs.m_path;
}

inline pointer& pointer::
operator/=(pointer_::object_key auto const& key)
{
    push_back(key);

    return *this;
}

inline pointer& pointer::
operator/=(array_index_type const array_index)
{
    push_back(array_index);

    return *this;
}

inline pointer
operator/(pointer const& p, pointer_::object_key auto const& key)
{
    auto result = p;

    result /= key;

    return result;
}

inline pointer
operator/(pointer_::object_key auto const& key, pointer const& p)
{
    auto result = p;

    result.insert(result.begin(), key);

    return result;
}

inline pointer
operator/(pointer const& p, pointer::array_index_type const index)
{
    auto result = p;

    result /= index;

    return result;
}

inline pointer
operator/(pointer::array_index_type const index, pointer const& p)
{
    auto result = p;

    result.insert(result.begin(), index);

    return result;
}

namespace literals {

    inline pointer operator ""_jp (char const* const ptr, size_t const len)
    {
        return json::pointer(std::string_view(ptr, len));
    }

} // namespace literals

} // namespace stream9::json

#endif // STREAM9_JSON_POINTER_IPP
