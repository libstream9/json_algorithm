#ifndef STREAM9_JSON_POINTER_HPP
#define STREAM9_JSON_POINTER_HPP

#include "namespace.hpp"

#include <stream9/json/value.hpp>
#include <stream9/json/value_from.hpp>

#include <compare>
#include <stdexcept>
#include <string>
#include <string_view>
#include <vector>

#include <stream9/ranges/range_facade.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9::json {

class pointer_view;

namespace pointer_ {

    using element_t = std::string; //TODO maybe immutable_string
    using path_t = std::vector<element_t>;
    using size_type = path_t::size_type; //TODO std::uint32_t;
    using difference_type = path_t::difference_type; //TODO std::int32_t;
    using array_index_type = stream9::safe_integer<difference_type, 0>;

    // prevent 0 from converting to string_view
    template<typename T>
    concept object_key = std::convertible_to<T, std::string_view>
                      && (!std::convertible_to<T, array_index_type>);

} // namespace pointer_

/**
 * RFC 6901 JSON Pointer
 *
 * @model std::regular
 * @model std::contiguous_range
 */
class pointer : public rng::range_facade<pointer>
{
public:
    using size_type = pointer_::size_type;
    using difference_type = pointer_::difference_type;
    using iterator = pointer_::path_t::iterator;
    using const_iterator = pointer_::path_t::const_iterator;
    using array_index_type = pointer_::array_index_type;

    enum class errc;
    class error;

public:
    pointer();

    pointer(std::string_view json_pointer);

    pointer(std::convertible_to<std::string_view> auto&& s)
        : pointer { std::string_view(s) }
    {}

    // accessor
    const_iterator begin() const;
    const_iterator end() const;

    // query
    size_type size() const;
    pointer_view slice(difference_type start) const;
    pointer_view slice(difference_type start, difference_type end) const;

    // modifier
    void push_back(array_index_type);

    void push_back(pointer_::object_key auto const& object_key);

    void pop_back();

    iterator insert(const_iterator pos, array_index_type array_index);

    iterator insert(const_iterator pos,
                    pointer_::object_key auto const& object_key);

    iterator erase(const_iterator pos);
    iterator erase(const_iterator first, const_iterator end);

    void clear();

    // conversion
    std::string to_string() const;
    explicit operator std::string () const;

    json::value to_value() const;
    explicit operator json::value () const;

    // comparison
    bool operator==(std::string_view) const;
    std::strong_ordering operator<=>(std::string_view) const;

    bool operator==(std::same_as<pointer> auto const& rhs) const;
    auto operator<=>(std::same_as<pointer> auto const& rhs) const;

    // operator
    pointer& operator/=(array_index_type array_index);

    pointer& operator/=(pointer_::object_key auto const&);

    friend pointer operator/(pointer const&, pointer_::object_key auto const&);

    friend pointer operator/(pointer_::object_key auto const&, pointer const&);

    friend pointer operator/(pointer const&, array_index_type array_index);
    friend pointer operator/(array_index_type array_index, pointer const&);

    // stream
    friend std::ostream& operator<<(std::ostream&, pointer const&);

private:
    pointer_::path_t m_path;
};

enum class pointer::errc {
    ok = 0,
    // parser
    incomplete_escape_seq = 100,
    unknown_escape_seq,
    not_start_with_slash,
    // walk
    pointer_index_out_of_bound = 200,
    invalid_array_index,
    array_index_has_leading_zero,
    object_key_does_not_exist,
    array_index_out_of_bound,
    array_index_on_null,
    object_key_on_null,
    array_index_on_scalar,
    object_key_on_scalar,
    // create
    object_key_already_exist = 300,
    array_value_already_exist,
    // insert
    insert_with_empty_pointer = 400,
    // erase
    erase_with_empty_pointer = 500,
    // extract
    extract_with_empty_pointer = 600,
    // move
    move_from_empty_pointer = 700,
    move_to_empty_pointer,
    move_source_is_ancestor_of_destination,
};

class pointer::error : public std::runtime_error
{
    using pointer_t = class pointer;
    using difference_type = pointer_t::difference_type;

public:
    error(errc, std::string_view pointer, char const* pos);
    error(errc, class pointer_view const&, difference_type p_idx);

    // accessor
    errc code() const { return m_ec; }
    std::string const& pointer() const { return m_pointer; }
    difference_type pos() const { return m_pos; }

    friend std::ostream& operator<<(std::ostream&, pointer::error const&);

private:
    errc m_ec;
    std::string m_pointer;
    difference_type m_pos;
};

std::ostream& operator<<(std::ostream&, pointer::errc);

// json::value_from support
void tag_invoke(json::value_from_tag, json::value&, pointer::error const&);

namespace literals {

    pointer operator ""_jp (char const* ptr, size_t len);

} // namespace literals

} // namespace stream9::json

#include "pointer.ipp"

#endif // STREAM9_JSON_POINTER_HPP
