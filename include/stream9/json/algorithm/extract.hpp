#ifndef STREAM9_JSON_ALGORITHM_EXTRACT_HPP
#define STREAM9_JSON_ALGORITHM_EXTRACT_HPP

#include "../pointer_view.hpp"

#include <stream9/json/value.hpp>

namespace stream9::json {

json::value
    extract(json::value& v,
            json::pointer_view const& path );

json::value
    extract(json::value&& v,
            json::pointer_view const& path );
/**********
# Effects
    Remove value that is pointed by path on `v` and return
    removed value.

# Preconditions
    1) `path` is not empty.
    2) On `v`, a path to the target value pointed by `path` exist.

# Returns
    extracted value

# Throws
    `class pointer::error`
        If precondition 1) is violated
            `extract_with_empty_pointer`
        If precondition 2) is violated
            `object_key_does_not_exist`
            `invalid_array_index`
            `array_index_out_of_bound`
            `array_index_has_leading_zero`
            `array_index_on_null`
            `object_key_on_null`
            `array_index_on_scalar`
            `object_key_on_scalar`

# Exception Safety
    strong exception safety

**********/

} // namespace stream9::json

#endif // STREAM9_JSON_ALGORITHM_EXTRACT_HPP
