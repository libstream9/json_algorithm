#ifndef STREAM9_JSON_ALGORITHM_INCLUDES_HPP
#define STREAM9_JSON_ALGORITHM_INCLUDES_HPP

#include <stream9/json/value.hpp>

namespace stream9::json {

bool includes(json::value const& v1, json::value const& v2);
/**********
# Effects
   Check whether `v1` includes `v2` or not.
   If both `v1` and `v2` are
     scalar: check `v1` == `v2`
     string: check `v1` == `v2`
     arrray: check if `v1` contains all elements of `v2`, position
             of elements are ignored.
     object: check if `v1` contains all elements of `v2`
   Check will be done recursively.

# Return value
   true if `v1` inclues `v2`

# Throws
    'class json::internal_error`
        underlining library throw some error (ex. out of memory)

# Complexity
    TODO

**********/

} // namespace stream9::json

#endif // STREAM9_JSON_ALGORITHM_INCLUDES_HPP
