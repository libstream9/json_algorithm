#ifndef STREAM9_JSON_POINTER_ALGORITHM_CREATE_PATH_HPP
#define STREAM9_JSON_POINTER_ALGORITHM_CREATE_PATH_HPP

#include "../pointer_view.hpp"

#include <stream9/json/value.hpp>
#include <stream9/json/value_from.hpp>

#include <iosfwd>

namespace stream9::json {

struct create_option {
    bool overwrite_null_on_the_path : 1 = true;
    bool overwrite_scalar_on_the_path : 1 = true;
    bool overwrite_array_when_token_is_not_array_index : 1 = true;
    bool create_empty_value_on_array : 1 = true;
    bool regard_all_token_of_absent_path_as_object_key : 1 = false;
};

std::ostream& operator<<(std::ostream&, create_option);
void tag_invoke(json::value_from_tag, json::value&, create_option);

json::value&
    create_path(json::value& root,
                pointer_view const& path,
                create_option const& = {});
/**********
# Effects
    Walk the `path` on the `root` and when it encouters roadblocks
    then craete a new value and proceed to the end.

# Parameters
    `opt`: option for the operation
        `overwrite_null_on_the_path` (default: true)
            Overwrite null with a new object or array when it
            encounter null on the `path`.
        `overwrite_scalar_on_the_path` (default: true)
            Overwrite scalar with a new object or array when it
            encounter scalar on the `path`.
         `overwrite_array_when_token_is_not_array_index` (default: true)
            Overwrite array with a new object if corresponding `path`
            token is not an array index.
         `create_empty_value_on_array` (default: true)
            Fill absent part of array with null value when array index
            extracted from `path` token is larger than the array size.
         `regard_all_token_of_absent_path_as_object_key` (default: false)
            Interpret all absent part of `path` tokens as object key when
            creating the new path, even if it does look like array index.

# Return value
    reference to a value at leaf (last) part of `path`

# Throws
    `class json::pointer::error`
        `array_index_on_null`
        `object_key_on_null`
            encountering null while `overwrite_null_on_the_path` is false

        `array_index_on_scalar`
        `object_key_on_scalar`
            encountering scalar while `overwrite_scalar_on_the_path` is false

        `invalid array index`
            ecountering object key on array while `overwrite_array_when_token_is_not_array_index`
            is false

        `array_index_out_of_bound`
            encountering array index that is larger than array's size
            while `create_empty_value_on_array` is false

    'class json::internal_error`
        underlining library throw some error (ex. out of memory)

# Exception Safety
    strong exception safety

# Complexity
    linear to the length of `path`

**********/

} // namespace stream9::json

#endif // STREAM9_JSON_POINTER_ALGORITHM_CREATE_PATH_HPP
