#ifndef STREAM9_JSON_ALGORITHM_MATCH_HPP
#define STREAM9_JSON_ALGORITHM_MATCH_HPP

#include "../pointer.hpp"

#include <stream9/function_ref.hpp>

namespace stream9::json {

class value;

using mismatch_callback = function_ref<
            bool(json::pointer const&/*p*/,
                 json::value const&/*x1*/, json::value const&/*x2*/ )>;

bool match(json::value const& v1, json::value const& v2, mismatch_callback);
/**********
# Effects
   Check whether `v1` matches against the pattern `v2`.
   If a value that doesn't match is found, callback will be called
   with following arguments
       - p
            pointer to the unmatched value
       - x1
            value on `v1`
       - x2
            value on `v2`
   Callback has to return boolean value which indicate whether you want
   to stop the matching (true) or keep going (false).

   Matching will be done as following manner.
   - If type of `v1` and `v2` are different they don't match.
   - If type of both `v1` and `v2` are
        - scalar or string
            Check `v1` == `v2`
        - arrray
            Check if `v1` contains all elements of `v2`. Position
            of elements are ignored.
        -object
            For all elements of `v2`, check whether `v1` contains
            element with a same key and a same value.
   - Matching will be done recursively.

# Return value
    true if all of pattern in `v2` matche with `v1`

# Throws
    'class json::internal_error`
        underlining library throw some error (ex. out of memory)

# Complexity
    TODO
**********/

struct match_result {
    bool match;
    json::pointer mismatch {};

    operator bool () const noexcept { return match; }
};

match_result match(json::value const& v1, json::value const& v2);
/**********
# Effects
   Check whether `v1` matches against the pattern `v2`

# Return value
    struct match_result {
        bool match; // true if all of pattern in `v2` matche with `v1`
        json::pointer mismatch; // empty if `match` is false, pointer to
                                // the first unmatched value othewise.

# Throws
    'class json::internal_error`
        underlining library throw some error (ex. out of memory)

# Complexity
    TODO
**********/

} // namespace stream9::json

#endif // STREAM9_JSON_ALGORITHM_MATCH_HPP
