#ifndef STREAM9_JSON_ALGORITHM_FIND_HPP
#define STREAM9_JSON_ALGORITHM_FIND_HPP

#include "../namespace.hpp"
#include "../pointer_view.hpp"

#include <stream9/json/value.hpp>

namespace stream9::json {

json::value*
    find(json::value& v,
         pointer_view const& path) noexcept;

json::value const*
    find(json::value const& v,
         pointer_view const& path) noexcept;
/**********
# Effects
    Find target value that is pointed by `path` on `v`.

# Return value
    A pointer to the target value, `nullptr` if path to the
    target value doesn't exist.

# Complexity
    linear to the length of `path`

**********/

json::value&
    find_or_throw(json::value& v,
                  pointer_view const& path);

json::value const&
    find_or_throw(json::value const& v,
                  pointer_view const& path);
/**********
# Effects
    Find value that is pointed by `path` on `v`.

# Preconditions
    A path to the target value has to exist on `v`

# Return value
    A pointer to the target value.

# Throws
    `class pointer::error`
        `object_key_does_not_exist`
        `invalid_array_index`
        `array_index_out_of_bound`
        `array_index_has_leading_zero`
        `array_index_on_null`
        `object_key_on_null`
        `array_index_on_scalar`
        `object_key_on_scalar`

# Exception Safety
    strong exception safety

# Complexity
    linear to the length of `path`

**********/

bool
    contains(json::value const& v,
             pointer_view const& path) noexcept;
/**********
# Effects
    Check if path to the value pointed by `path` exist on `v`

# Return value
    `true` if path exist, `false` otherwise

# Complexity
    linear to the length of `path`

**********/

} // namespace stream9::json

#endif // STREAM9_JSON_ALGORITHM_FIND_HPP

#include "find.ipp"
