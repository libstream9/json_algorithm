#ifndef STREAM9_JSON_ALGORITHM_REPLACE_HPP
#define STREAM9_JSON_ALGORITHM_REPLACE_HPP

#include "insert.hpp"

#include "../pointer_view.hpp"

#include <stream9/json/value.hpp>

namespace stream9::json {

json::value&
    replace(json::value& v1,
            pointer_view const& path,
            json::value const& v2 );

json::value&
    replace(json::value& v1,
            pointer_view const& path,
            json::value&& v2 );
/**********
# Effects
    Replace the target value that is pointed by `path` on `v1`
    with the value `v2`.

# Precondition
    1) On `v1`, a path to the target value pointed by `path` has to exist.

# Return value
    reference to the replaced value

# Throws
    `class pointer::error`
        `object_key_does_not_exist`
        `invalid_array_index`
        `array_index_out_of_bound`
        `array_index_has_leading_zero`
        `array_index_on_null`
        `object_key_on_null`
        `array_index_on_scalar`
        `object_key_on_scalar`

    'class json::internal_error`
        underlining library throw some error (ex. out of memory)

# Exception Safety
    strong exception safety

# Complexity
    linear to the length `path`

**********/

json::value&
    replace_or_insert(json::value& v1,
                      pointer_view const& path,
                      json::value const& v2,
                      insert_option const& = {} );
json::value&
    replace_or_insert(json::value& v1,
                      pointer_view const& path,
                      json::value&& v2,
                      insert_option const& = {} );
/**********
# Effects
    Replace the target value that is pointed by `path` on `v1`
    with the value `v2`.
    If target value doesn't exist, insert `v2` instead.

# Precondition
    1) On `v1`, a path to the parent of target pointed by `path` exist.
       ex) If `path` is `/a/b/c` then `/a/b` has to exist.
    2) On `v1`, the parent is either object or array or null.
    3) On `v1`, If the parent is array,
       i) last token of `path` is proper array index.
       ii) array index of i) is less than size of the array.
    4) if the parent value is null and last token of `path` is array index
       - array index is 0 unless `create_empty_value_on_array` is true
    5) if `regard_last_token_of_path_as_object_key` is true
       - the parent value is object

# Parameters
    `opt`: option for inserting (see "insert.hpp" for detail)

# Return value
    reference to the replaced or inserted value

# Exceptions
    `class pointer::error`
        if precondition 1) is violated
            `object_key_does_not_exist`
            `invalid_array_index`
            `array_index_out_of_bound`
            `array_index_has_leading_zero`
            `array_index_on_null`
            `object_key_on_null`
            `array_index_on_scalar`
            `object_key_on_scalar`
        if precondition 2) is violated
            `array_index_on_scalar`
            `object_key_on_scalar`
        if precondition 3) is violated
            `invalid_array_index`
            `array_index_out_of_bound`
            `array_index_has_leading_zero`

    'class json::internal_error`
        underlining library throw some error (ex. out of memory)

# Exception Safety
    strong exception safety

# Complexity
    linear to the length `path`

**********/

} // namespace stream9::json

#endif // STREAM9_JSON_ALGORITHM_REPLACE_HPP
