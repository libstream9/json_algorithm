#ifndef STREAM9_JSON_ALGORITHM_WALK_HPP
#define STREAM9_JSON_ALGORITHM_WALK_HPP

#include "../pointer_view.hpp"

#include <stream9/json/value.hpp>

namespace stream9::json {

struct walk_result {
    pointer::errc ec = {};
    json::value& value;
    pointer_view::difference_type n_step;
};

walk_result
    walk(json::value& v,
         json::pointer_view const& path) noexcept;
/**********
# Effects
    Walk on the value `v` to the target that is pointed by `path`
    until it encounters some roadblocks.

# Return value
    `ec`: error code, `ok` on success
          `object_key_does_not_exist`
          `invalid_array_index`
          `array_index_out_of_bound`
          `array_index_has_leading_zero`
          `array_index_on_null`
          `object_key_on_null`
          `array_index_on_scalar`
          `object_key_on_scalar`
    `value`: reference to the last value to be stepped on
    `n_step`: number of step taken (aka index to the token on `path` that
              corresponds to `value`)

# Complexity
    linear to the length of `path`

**********/

} // namespace stream9::json

#endif // STREAM9_JSON_ALGORITHM_WALK_HPP
