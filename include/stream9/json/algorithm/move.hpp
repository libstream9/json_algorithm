#ifndef STREAM9_JSON_ALGORITHM_MOVE_HPP
#define STREAM9_JSON_ALGORITHM_MOVE_HPP

#include "insert.hpp"

#include "../namespace.hpp"
#include "../pointer.hpp"

#include <stream9/json/value.hpp>

#include <iosfwd>

namespace stream9::json {

enum class move_method {
    insert = 0,
    replace,
    insert_or_replace,
    replace_or_insert
};

std::ostream& operator<<(std::ostream&, move_method);

json::value&
    move(json::value& v1,
         json::pointer_view const& src,
         json::value& v2,
         json::pointer_view const& dest,
         move_method const method = move_method::insert,
         insert_option const& opt = {} );
/**********
# Effects
    move the target value on `v1` that is pointed by `src`
    to the location on `v2` that is pointed by `dest`

# Preconditions
    1) Both `src` and `dest` are not empty (You can't remove/insert value
       from/to itself).
    2) If `v1` and `v2` is identical, `src` is not proper prefix of `dest`
       (You can't move value into its descendent).
    3) On `v1`, there is a path to the target (`src`).
    4) On `v2`, there is a path to the parent of target (`dest.slice(0, -1)`).
    5) On `v2`, the parent value is either object or array or null.
    6) On `v2`, if the parent value is array
       - last token of `dest` is proper array index
       - array index is not larger than size of the array
         (when index is equal to size of the array, target value on `v1`
         will be inserted to the back of the array)
    7) On `v2`, if the parent value is null and last token of `path` is
       array index
       - array index is 0 unless `create_empty_value_on_array` is true
    8) If `regard_last_token_of_path_as_object_key` is true then the parent
       value on `v2` is object.

# Parameters
    `method`: Specify how to move target value on `v1` to `v2`
        `insert`
        `replace`
        `insert_or_replace`
        `replace_or_insert`
    `opt`: option for insert operation (see "insert.hpp" for detail)

# Return value
    reference to the moved target value

# Throws
    `class pointer::error`
        if precondition 1) is violated
            `move_from_empty_pointer`
            `move_to_empty_pointer`

        if precondition 2) is violated
            `move_source_is_ancestor_of_destination`

        if precondition 3) or 4) is violated
            `object_key_does_not_exist`
            `invalid_array_index`
            `array_index_out_of_bound`
            `array_index_has_leading_zero`
            `array_index_on_null`
            `object_key_on_null`
            `array_index_on_scalar`
            `object_key_on_scalar`

        if precondition 5) is violated
            `array_index_on_scalar`
            `object_key_on_scalar`

        if precondition 6) is violated
            `invalid_array_index`
            `array_index_out_of_bound`
            `array_index_has_leading_zero`

        if precondition 7) is violated
            `array_index_out_of_bound`

        if precondition 8) is violated
            `invalid_array_index`

    'class json::internal_error`
        underlining library throw some error (ex. out of memory)

# Exception Safety
    strong exception safety

# Complexity
    linear to length of `src` + length of `dest`

**********/

} // namespace stream9::json

#endif // STREAM9_JSON_ALGORITHM_MOVE_HPP
