#ifndef STREAM9_JSON_ALGORITHM_MERGE_HPP
#define STREAM9_JSON_ALGORITHM_MERGE_HPP

#include <stream9/json/value.hpp>

namespace stream9::json {

[[nodiscard]] json::value
    merge(json::value const& target, json::value const& patch);
/**********
# Effects
   Apply `patch` to `target`
   see detail on [RFC7386 JSON merge patch]

# Return value
    merges value

# Throws
    'class json::internal_error`
        underlining library throw some error (ex. out of memory)

# Complexity
    linear to the number of values in `patch`

**********/

} // namespace stream9::json

#endif // STREAM9_JSON_ALGORITHM_MERGE_HPP
