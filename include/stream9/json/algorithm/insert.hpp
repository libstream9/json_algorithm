#ifndef STREAM9_JSON_ALGORITHM_INSERT_HPP
#define STREAM9_JSON_ALGORITHM_INSERT_HPP

#include "../pointer_view.hpp"

#include <stream9/json/value.hpp>
#include <stream9/json/value_from.hpp>

namespace stream9::json {

struct insert_option {
    bool create_new_value_on_null : 1 = true;
    bool create_empty_value_on_array : 1 = false;
    bool regard_last_token_of_path_as_object_key : 1 = false;
};

json::value&
    insert(json::value& v1,
           pointer_view const& path,
           json::value const& v2,
           insert_option const& opt = {});

json::value&
    insert(json::value& v1,
           pointer_view const& path,
           json::value&& v2,
           insert_option const& opt = {});
/**********
# Effects
    Insert value `v2` to value `v1` at the target pointed by `path`.
    If the parent of target value is null then emplace new object or
    array and insert `v2` on it.

# Preconditions
    1) `path` is not empty. (can't insert to the value itself)
    2) On `v1`, path to the parent of target value exists
    3) the parent value is either object or array or null
    4) if the parent value is object
       - last token of `path` doesn't already exist in the object
    5) if the parent value is array
       - last token of `path` is proper array index
       - array index is not larger than size of the array
         (when index is equal to size of the array, `v2` will be inserted
         to the back of the array)
    6) if the parent value is null and last token of `path` is array index
       - array index is 0 unless `create_empty_value_on_array` is true
    7) if `regard_last_token_of_path_as_object_key` is true
       - the parent value is object

# Parameters
    `opt`: option for insert operation
        `create_new_value_on_null` (default: true)
            If target value is null, overwrite it with new value and
            insert `v2` into it.
        `create_empty_value_on_array` (default: false)
            Fill absent part of array with null value when array index
            of target location is larger than the array size.
        `regard_last_token_of_path_as_object_key` (default: false)
            Interpret last token of `path` as object key even if it
            does look like array index.

# Return value
    reference to the inserted value

# Throws
    `json::pointer::error`
        when precondition 1) is violated
            `insert_with_empty_pointer`

        when precondition 2) is violated
            `object_key_does_not_exist`
            `invalid_array_index`
            `array_index_out_of_bound`
            `array_index_has_leading_zero`
            `object_key_on_null`
            `array_index_on_null`
            `object_key_on_scalar`
            `array_index_on_scalar`

        when precondition 3) is violated
            `array_index_on_scalar`
            `object_key_on_scalar`

        when precondition 4) is violated
            `object_key_already_exist`

        when precondition 5) is violated
            `invalid_array_index`
            `array_index_out_of_bound`
            `array_index_has_leading_zero`

        when precondition 6) is violated
            `array_index_out_of_bound`

        when precondition 7) is violated
            `invalid_array_index`

    'class json::internal_error`
        underlining library throw some error (ex. out of memory)

# Exception Safety
    strong exception safety

# Complexity
    linear to the length of `path`

**********/

json::value&
    insert_or_replace(json::value& v1,
                      pointer_view const& path,
                      json::value const& v2,
                      insert_option const& = {});

json::value&
    insert_or_replace(json::value& v1,
                      pointer_view const& path,
                      json::value&& v2,
                      insert_option const& = {});
/**********
# Effects
    Insert value `v2` to value `v1` at the target pointed by `path`.
    If insert fail because of key on insert location already exist
    replace it instead.

**********/

// json::value_from support
void tag_invoke(json::value_from_tag, json::value&, insert_option const&);

} // namespace stream9::json

#endif // STREAM9_JSON_ALGORITHM_INSERT_HPP
