#ifndef STREAM9_JSON_ALGORITHM_DIFF_HPP
#define STREAM9_JSON_ALGORITHM_DIFF_HPP

#include "../namespace.hpp"
#include "../pointer.hpp"

#include <stream9/json/value.hpp>

namespace stream9::json {

struct diff_event_handler
{
    virtual ~diff_event_handler() = default;

    virtual void add(pointer const&, json::value const&) {}
    virtual void remove(pointer const&, json::value const&) {}

    virtual void replace(pointer const&,
                         json::value const&/*from*/, json::value const&/*to*/) {}

    virtual void move(pointer const&/*from*/,
                      json::value const&, pointer const&/*to*/) {}
};

void diff(json::value const& v1,
          json::value const& v2,
          diff_event_handler& );
// # Synopsis
//     Compute minimum edit sequence from `v1` to `v2`

} // namespace stream9::json

#endif // STREAM9_JSON_ALGORITHM_DIFF_HPP
