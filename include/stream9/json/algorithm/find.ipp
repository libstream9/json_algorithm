#ifndef STREAM9_JSON_ALGORITHM_FIND_IPP
#define STREAM9_JSON_ALGORITHM_FIND_IPP

namespace stream9::json {

inline json::value const*
find(json::value const& v, pointer_view const& p) noexcept
{
    return const_cast<json::value const*>(
        find(const_cast<json::value&>(v), p) );
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdangling-reference" // GCC 13.1.1 generate false positive
inline json::value const&
find_or_throw(json::value const& v, pointer_view const& p)
{
    return const_cast<json::value const&>(
        find_or_throw(const_cast<json::value&>(v), p) );
}
#pragma GCC diagnostic pop

inline bool
contains(json::value const& v, pointer_view const& p) noexcept
{
    return find(v, p) != nullptr;
}

} // namespace stream9::json

#endif // STREAM9_JSON_ALGORITHM_FIND_IPP
