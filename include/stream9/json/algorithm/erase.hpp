#ifndef STREAM9_JSON_ALGORITHM_ERASE_HPP
#define STREAM9_JSON_ALGORITHM_ERASE_HPP

#include "../pointer_view.hpp"

#include <stream9/json/value.hpp>

namespace stream9::json {

json::value&
    erase(json::value& v,
          pointer_view const& p);
/**********
# Effects
    Erase the target value that is pointed by `path` on `v`.

# Preconditions
    1) `path` must not an empty pointer (can't erase value itself)
    2) On `v`, a path to the target value has to exist.

# Return value
    reference to the parent of erased value

# Throws
    `class pointer::error`
        when precondition 1) is violated
            `erase_with_empty_pointer`

        when precondition 2) is violated
            `object_key_does_not_exist`
            `invalid_array_index`
            `array_index_out_of_bound`
            `array_index_has_leading_zero`
            `array_index_on_scalar`
            `object_key_on_scalar`
            `array_index_on_null`
            `object_key_on_null`

# Exception Safety
    strong exception safety

# Complexity
    linear to the length of `p`

**********/

} // namespace stream9::json

#endif // STREAM9_JSON_ALGORITHM_ERASE_HPP
