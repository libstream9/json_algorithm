#ifndef STREAM9_JSON_ALGORITHM_HPP
#define STREAM9_JSON_ALGORITHM_HPP

#include <stream9/json.hpp>

//
// RFC6901 JSON Pointer
//
#include "pointer.hpp"
#include "pointer_view.hpp"

//
// RFC6902 JSON Patch
//
#include "patch.hpp"

//
// JSON Algorithms
//

// create_path()
//     craete the path to the value that is indicated by JSON pointer
#include "algorithm/create_path.hpp"

// walk()
//     walk the path on value that is indicated by JSON pointer
#include "algorithm/walk.hpp"

// find()
//     find the value with JSON pointer
#include "algorithm/find.hpp"

// insert()
//     insert value at the place pointed by JSON pointer
#include "algorithm/insert.hpp"

// erase()
//     erase value at the place pointed by JSON pointer
#include "algorithm/erase.hpp"

// extract()
//     extract value from the place pointed by JSON pointer
#include "algorithm/extract.hpp"

// replace()
//     replace value at the place pointed by JSON pointer
#include "algorithm/replace.hpp"

// move()
//     move value at the place pointed by JSON pointer to another place
#include "algorithm/move.hpp"

// diff()
//     compute difference between two value
#include "algorithm/diff.hpp"

// merge()
//     RFC7386 JSON Merge Patch
#include "algorithm/merge.hpp"

// includes()
//      predicate to check a value includes another value
#include "algorithm/includes.hpp"

// match()
//      patten match
#include "algorithm/match.hpp"

#endif // STREAM9_JSON_ALGORITHM_HPP
