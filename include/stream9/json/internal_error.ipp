#ifndef STREAM9_JSON_INTERNAL_ERROR_IPP
#define STREAM9_JSON_INTERNAL_ERROR_IPP

namespace stream9::json {

inline internal_error::
internal_error(json::value&& context)
    : std::runtime_error { "internal_error" }
    , m_context { std::move(context) }
    , m_nested { std::current_exception() }
{}

inline json::value const& internal_error::
context() const
{
    return m_context;
}

[[noreturn]] inline void internal_error::
rethrow_nested() const
{
    std::rethrow_exception(m_nested);
}

} // namespace stream9::json

#endif // STREAM9_JSON_INTERNAL_ERROR_IPP
