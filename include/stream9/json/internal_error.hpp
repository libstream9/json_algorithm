#ifndef STREAM9_JSON_INTERNAL_ERROR_HPP
#define STREAM9_JSON_INTERNAL_ERROR_HPP

#include <stream9/json/value.hpp>
#include <stream9/json/value_from.hpp>

#include <exception>
#include <stdexcept>

namespace stream9::json {

class internal_error : public std::runtime_error
{
public:
    internal_error(json::value&& context);

    // query
    json::value const& context() const;
    [[noreturn]] void rethrow_nested() const;

private:
    json::value m_context;
    std::exception_ptr m_nested;
};

// json::value_from support
void tag_invoke(json::value_from_tag, json::value&, internal_error const&);

} // namespace stream9::json

#include "internal_error.ipp"

#endif // STREAM9_JSON_INTERNAL_ERROR_HPP
