#ifndef STREAM9_JSON_NAMESPACE_HPP
#define STREAM9_JSON_NAMESPACE_HPP

namespace std::ranges {}
namespace std::ranges::views {}
namespace stream9::ranges {}
namespace stream9::ranges::views {}
namespace stream9::strings {}
namespace stream9::iterators {}

namespace stream9::json {

namespace rng { using namespace std::ranges; }
namespace rng::views { using namespace std::ranges::views; }

namespace rng { using namespace stream9::ranges; }
namespace rng::views { using namespace stream9::ranges::views; }

namespace str { using namespace stream9::strings; }

namespace iter { using namespace stream9::iterators; }

} // namespace stream9::json

#endif // STREAM9_JSON_NAMESPACE_HPP
