#ifndef STREAM9_JSON_POINTER_VIEW_HPP
#define STREAM9_JSON_POINTER_VIEW_HPP

#include "pointer.hpp"

#include <stream9/json/value.hpp>

#include <compare>
#include <iosfwd>
#include <string>
#include <string_view>

#include <stream9/ranges/range_facade.hpp>

namespace stream9::json {

class pointer_view : public rng::range_facade<pointer_view>
{
public:
    using size_type = pointer::size_type;
    using difference_type = pointer::difference_type;
    using const_iterator = pointer::const_iterator;

public:
    pointer_view() = default;

    pointer_view(pointer const&);
    pointer_view(pointer const&, difference_type offset);
    pointer_view(pointer const&, difference_type offset, difference_type length);

    // accessor
    const_iterator begin() const;
    const_iterator end() const;

    // query
    size_type size() const;

    pointer const* base() const;
    difference_type offset() const;

    pointer_view slice(difference_type start) const;
    pointer_view slice(difference_type start, difference_type end) const;

    // conversion
    std::string to_string() const;
    explicit operator std::string () const;

    json::value to_value() const;
    explicit operator json::value () const;

    // comparison
    bool operator==(pointer_view const&) const;
    std::strong_ordering operator<=>(pointer_view const&) const;

    bool operator==(std::string_view) const;
    std::strong_ordering operator<=>(std::string_view) const;

    // stream
    friend std::ostream& operator<<(std::ostream&, pointer_view const&);

private:
    pointer const* m_base;
    difference_type m_offset = 0;
    difference_type m_length = 0;
};

/*
 * definition
 */
inline pointer_view::
pointer_view(pointer const& p)
    : m_base { &p }
    , m_length { p.ssize() }
{}

inline pointer_view::
pointer_view(pointer const& p, difference_type const offset)
    : m_base { &p }
    , m_offset { offset }
    , m_length { p.ssize() - offset }
{
    assert(m_offset + m_length <= p.ssize());
}

inline pointer_view::
pointer_view(pointer const& p, difference_type const offset,
                               difference_type const length)
    : m_base { &p }
    , m_offset { offset }
    , m_length { length }
{
    assert(m_offset + m_length <= p.ssize());
}

inline pointer_view::const_iterator pointer_view::
begin() const
{
    if (m_base) {
        return m_base->begin() + m_offset;
    }
    else {
        return {};
    }
}

inline pointer_view::const_iterator pointer_view::
end() const
{
    if (m_base) {
        return begin() + m_length;
    }
    else {
        return {};
    }
}

inline pointer_view::size_type pointer_view::
size() const
{
    return static_cast<size_type>(m_length);
}

inline pointer const* pointer_view::
base() const
{
    return m_base;
}

inline pointer_view::difference_type pointer_view::
offset() const
{
    return m_offset;
}

inline pointer_view::
operator std::string() const
{
    return to_string();
}

inline pointer_view::
operator json::value() const
{
    return to_value();
}

} // namespace stream9::json

#endif // STREAM9_JSON_POINTER_VIEW_HPP
