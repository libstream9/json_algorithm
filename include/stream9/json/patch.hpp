#ifndef STREAM9_JSON_PATCH_HPP
#define STREAM9_JSON_PATCH_HPP

#include "namespace.hpp"
#include "pointer_view.hpp"

#include <stream9/json/value.hpp>
#include <stream9/json/type_traits.hpp>

#include <exception>
#include <iosfwd>
#include <stdexcept>

#include <stream9/ranges/range_facade.hpp>

namespace stream9::json {

/*
 * RFC6902 JSON Patch
 *
 * @model std::regular;
 * @model rng::random_access_range
 */
class patch : public rng::range_facade<patch>
{
public:
    using const_iterator = json::array::const_iterator;
    using size_type = json::array::size_type;

    enum class errc;
    class error;

public:
    patch();

    // from json document
    patch(std::string_view);

    // from pre-parsed json value
    patch(same_without_cvref<json::value> auto&& v)
        : patch { std::forward<decltype(v)>(v), true }
    {}

    // by computing diff from v1 to v2
    patch(json::value const& v1, json::value const& v2);

    // accessor
    const_iterator begin() const;
    const_iterator end() const;

    json::value const& value() const { return m_value; }

    // query
    size_type size() const;

    // modifier
    patch& add(json::pointer_view const&, json::value const&);
    patch& remove(json::pointer_view const&);
    patch& replace(json::pointer_view const&, json::value const&);
    patch& move(json::pointer_view const& src, json::pointer_view const& dest);
    patch& copy(json::pointer_view const& src, json::pointer_view const& dest);
    patch& test(json::pointer_view const&, json::value const&);

    // conversion
    operator json::value const& () const { return value(); }

    // comparison
    bool operator==(patch const&) const = default;
    bool operator==(json::value const&) const;

    friend std::ostream& operator<<(std::ostream&, patch const&);

private:
    patch(json::value, bool);

private:
    json::value m_value;
};

/*
 * non-member function
 */
[[nodiscard]] json::value
    apply_patch(json::value const& v, patch const& p);
/**********
# Synopsis
    Apply patch `p` to value `v`

# Preconditions
    For each operations in `p` hold following precondition
    `add` operation
        precondition of json::insert_or_replace() holds (see "insert.hpp")
    `remove` operation
        precondition of json::erase() holds (see "erase.hpp")
    `replace` operation
        precondition of json::replace() holds (see "replace.hpp")
    `move` operation
        precondition of json::move() holds (see "move.hpp")
    `copy` operation
        precndition of json::find_or_throw() and json::insert_or_replace()
        hold (see "find.hpp" and "insert.hpp")
    `test` operation
        precondition of json::find_or_throw() (see "find.hpp")
        the target value on `v` pointed by `path` is equal to the `value`

# Return value
    a value which patch is applied

# Exceptions
    `json::patch::error`
        `pointer_error`
            If preconditions of underlined functions are violated
        `tested_value_does_not_match`
            If `test` operation fail

    'class json::internal_error`
        underlining library throw some error (ex. out of memory)

**********/

/*
 * class patch::error
 */
enum class patch::errc {
    ok = 0,
    pointer_error,
    internal_error,
    // patch semantic error
    patch_document_has_to_be_array = 100,
    operation_is_not_object,
    operation_does_not_contain_op_member,
    op_member_has_to_be_string,
    operation_does_not_contain_path_member,
    path_member_has_to_be_string,
    path_member_is_not_proper_json_pointer,
    operation_does_not_contain_from_member,
    from_member_has_to_be_string,
    from_member_is_not_proper_json_pointer,
    operation_does_not_contain_value_member,
    empty_path_on_remove_operation,
    from_location_can_not_be_proper_prefix_of_path_location,
    // operand semantic error
    tested_value_does_not_match = 200,
};

class patch::error : public std::runtime_error
{
public:
    error(errc, json::value const&);
    error(errc, json::value const&, std::exception_ptr const& nested);

    // accessor
    errc const& code() const;
    json::value const& value() const;

    // query
    void rethrow_if_nested() const;

private:
    errc m_ec;
    json::value m_value;
    std::exception_ptr m_nested = nullptr;
};

std::ostream& operator<<(std::ostream&, patch::errc);
std::ostream& operator<<(std::ostream&, patch::error const&);

// json::value_from support
void tag_invoke(json::value_from_tag, json::value&, patch::error const&);

/*
 * definition
 */
inline bool patch::
operator==(json::value const& v) const
{
    return v == m_value;
}


inline patch::errc const& patch::error::
code() const
{
    return m_ec;
}

inline json::value const& patch::error::
value() const
{
    return m_value;
}

inline void patch::error::
rethrow_if_nested() const
{
    if (m_nested) {
        std::rethrow_exception(m_nested);
    }
}

} // namespace stream9::json

#endif // STREAM9_JSON_PATCH_HPP
