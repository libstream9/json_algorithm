#include <stream9/json/internal_error.hpp>

#include <boost/core/demangle.hpp>

namespace stream9::json {

void
tag_invoke(json::value_from_tag,
           json::value& v,
           internal_error const& e)
{
    auto& obj = v.emplace_object();
    obj["context"] = e.context();

    try {
        e.rethrow_nested();
    }
    catch (std::exception const& e) {
        json::object nested;
        nested["type"] = boost::core::demangle(typeid(e).name());
        nested["what"] = e.what();

        obj["nested"] = std::move(nested);
    }
    catch (...) {
        obj["nested"] = "unknown error";
    }
}

} // namespace stream9::json
