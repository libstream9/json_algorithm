#include <stream9/json/pointer_view.hpp>

#include <ostream>

namespace stream9::json {

namespace  {

static void
escape_element(std::string& result, std::string_view const element)
{
    result.clear();
    result.reserve(element.size() + 5);

    for (auto const c: element) {
        if (c == '~') {
            result.append("~0");
        }
        else if (c == '/') {
            result.append("~1");
        }
        else {
            result.push_back(c);
        }
    }
}

template<typename T>
void
copy_to_string(T& result, pointer_view const& p)
{
    size_t len = 0;
    for (auto const& el: p) {
        len += el.size() + 1;
    }

    result.reserve(len);

    for (auto const& el: p) {
        result.push_back('/');
        result.append(el);
    }
}

} // anonymous namespace

/*
 * class pointer_view
 */
pointer_view pointer_view::
slice(difference_type const s) const
{
    return slice(s, this->ssize());
}

pointer_view pointer_view::
slice(difference_type const s, difference_type const e) const
{
    if (!m_base) return {};

    auto const sz = m_base->ssize();

    auto start = s < 0 ? sz + s : m_offset + s;
    if (start >= sz) {
        return {};
    }
    else if (start < 0) {
        start = 0;
    }

    auto end = e < 0 ? sz + e : m_offset + e;
    if (end > sz) {
        end = sz;
    }
    else if (end < 0) {
        end = 0;
    }

    auto const len = end - start;
    if (len < 0) {
        return {};
    }

    assert(start >= 0);
    assert(len >= 0);

    return { *m_base, start, len };
}

std::string pointer_view::
to_string() const
{
    std::string result;

    copy_to_string(result, *this);

    return result;
}

json::value pointer_view::
to_value() const
{
    json::string result;

    copy_to_string(result, *this);

    return result;
}

bool pointer_view::
operator==(pointer_view const& other) const
{
    return rng::equal(*this, other);
}

std::strong_ordering pointer_view::
operator<=>(pointer_view const& other) const
{
    return std::lexicographical_compare_three_way(
        begin(), end(),
        other.begin(), other.end()
    );
}

bool pointer_view::
operator==(std::string_view s) const
{
    if (s.empty()) return empty();

    std::string lhs;
    for (auto const& e: *this) {
        if (s.empty()) return false;

        if (s[0] != '/') return false;
        s.remove_prefix(1); // skip '/'

        escape_element(lhs, e);

        if (!s.starts_with(lhs)) return false;
        s.remove_prefix(lhs.size()); // skip path
    }

    return s.empty();
}

std::strong_ordering pointer_view::
operator<=>(std::string_view s) const
{
    if (s.empty()) {
        if (empty()) {
            return std::strong_ordering::equal;
        }
        else {
            return std::strong_ordering::greater;
        }
    }
    else {
        std::string lhs;
        for (auto const& e: *this) {
            if (s.empty()) return std::strong_ordering::greater;

            if (s[0] != '/') return '/' <=> s[0];
            s.remove_prefix(1); // skip '/'

            escape_element(lhs, e);

            auto const pos = s.find_first_of('/');
            auto const len = pos == s.npos ? s.size() : pos;
            auto const rhs = s.substr(0, len);

            if (auto comp = lhs <=> rhs; std::is_neq(comp)) {
                return comp;
            }

            s.remove_prefix(len); // skip element
        }

        return s.empty() ? std::strong_ordering::equal
                         : std::strong_ordering::greater;
    }
}

std::ostream&
operator<<(std::ostream& os, pointer_view const& p)
{
    std::string encoded;
    for (auto const& el: p) {
        escape_element(encoded, el);
        os << '/' << encoded;
    }

    return os;
}

} // namespace stream9::json
