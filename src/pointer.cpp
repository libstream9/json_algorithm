#include <stream9/json/pointer.hpp>

#include <stream9/json/namespace.hpp>
#include <stream9/json/pointer_view.hpp>

#include <algorithm>
#include <cctype>
#include <ranges>
#include <string>
#include <string_view>

#include <stream9/ranges/index_at.hpp>
#include <stream9/to_string.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9::json {

static std::string
unescape_element(std::string_view const input, std::string_view const element)
{
    using errc = pointer::errc;

    std::string result;

    auto it = element.begin();
    auto const end = element.end();

    for (; it != end; ++it) {
        if (*it == '~') {
            ++it;
            if (it == end) {
                throw pointer::error(errc::incomplete_escape_seq, input, it);
            }
            else if (*it == '0') {
                result.push_back('~');
            }
            else if (*it == '1') {
                result.push_back('/');
            }
            else {
                throw pointer::error(errc::unknown_escape_seq, input, it);
            }
        }
        else {
            result.push_back(*it);
        }
    }

    return result;
}

static void
parse(std::vector<std::string>& path,
      std::string_view const input,
      std::string_view::const_iterator& it )
{
    using errc = pointer::errc;

    if (it == input.end()) return;

    if (*it != '/') {
        throw pointer::error(errc::not_start_with_slash, input, it);
    }
    ++it; // skip '/'

    stream9::safe_integer const idx = rng::index_at(input, it);
    auto const pos = input.find_first_of('/', idx);

    auto const element =
        pos == input.npos ? std::string_view(it, input.end())
                          : std::string_view(it, input.begin() + pos);

    path.emplace_back(unescape_element(input, element));

    it += element.size();

    parse(path, input, it);
}

static void
parse(std::vector<std::string>& path,
      std::string_view const input)
{
    auto it = input.begin();
    parse(path, input, it);
}

/*
 * pointer
 */
pointer::
pointer(std::string_view const s)
{
    parse(m_path, s);
}

pointer_view pointer::
slice(difference_type const start) const
{
    return pointer_view(*this).slice(start);
}

pointer_view pointer::
slice(difference_type const start, difference_type const end) const
{
    return pointer_view(*this).slice(start, end);
}

std::string pointer::
to_string() const
{
    return pointer_view(*this).to_string();
}

json::value pointer::
to_value() const
{
    return pointer_view(*this).to_value();
}

bool pointer::
operator==(std::string_view s) const
{
    return pointer_view(*this) == s;
}

std::strong_ordering pointer::
operator<=>(std::string_view s) const
{
    return pointer_view(*this) <=> s;
}

std::ostream&
operator<<(std::ostream& os, pointer const& p)
{
    return os << pointer_view(p);
}

/*
 * error
 */
pointer::error::
error(errc const e,
      std::string_view const pointer, char const* const pos)
    : std::runtime_error { str::to_string(e) }
    , m_ec { e }
    , m_pointer { pointer }
    , m_pos { rng::index_at(pointer, pos) }
{}

pointer::error::
error(errc const e,
      class pointer_view const& p, difference_type p_idx)
    : std::runtime_error { str::to_string(e) }
    , m_ec { e }
{
    if (p_idx < 0) {
        p_idx = p.ssize() + p_idx;
    }

    if (p_idx < 0) p_idx = 0;
    if (p_idx >= p.ssize()) p_idx = p.ssize() - 1;

    auto* const base = p.base();
    if (!base) {
        m_pos = 0;
    }
    else {
        if (base->empty()) {
            m_pos = 0;
        }
        else {
            m_pointer = base->to_string();
            auto const base_idx = p.offset() + p_idx;

            stream9::safe_integer<size_t> pos = 0;
            for (difference_type i = 0; i < base_idx; ++i) {
                pos = m_pointer.find_first_of('/', pos + 1);
            }

            m_pos = pos + 1;
        }
    }
}

static std::string_view
message(pointer::errc const e)
{
    using E = pointer::errc;

    switch (static_cast<E>(e)) {
        // general
        case E::ok:
            return "ok";
        // parser
        case E::incomplete_escape_seq:
            return "incomplete escape sequence";
        case E::unknown_escape_seq:
            return "unknown escape sequence";
        case E::not_start_with_slash:
            return "pointer has to start with '/'";
        // walk
        case E::pointer_index_out_of_bound:
            return "pointer index out of bound";
        case E::invalid_array_index:
            return "invalid array index";
        case E::array_index_has_leading_zero:
            return "array index has leading zero";
        case E::object_key_does_not_exist:
            return "object key does not exist";
        case E::array_index_out_of_bound:
            return "array index out of bound";
        case E::array_index_on_null:
            return "array index on null";
        case E::object_key_on_null:
            return "object key on null";
        case E::array_index_on_scalar:
            return "array index on scalar";
        case E::object_key_on_scalar:
            return "object key on scalar";
        // create
        case E::object_key_already_exist:
            return "object key already exist";
        case E::array_value_already_exist:
            return "array value already exist";
        // insert
        case E::insert_with_empty_pointer:
            return "insert with empty pointer";
        // erase
        case E::erase_with_empty_pointer:
            return "erase with empty pointer";
        // extract
        case E::extract_with_empty_pointer:
            return "extract with empty pointer";
        // move
        case E::move_from_empty_pointer:
            return "move from empty pointer";
        case E::move_to_empty_pointer:
            return "move to empty pointer";
        case E::move_source_is_ancestor_of_destination:
            return "move source is ancestor of destination";
    }

    return "unknown error";
}

std::ostream&
operator<<(std::ostream& os, pointer::errc const e)
{
    return os << message(e);
}

std::ostream&
operator<<(std::ostream& os, pointer::error const& e)
{
    os << json::value_from(e);

    return os;
}

void
tag_invoke(json::value_from_tag, json::value& v, pointer::error const& e)
{
    auto& obj = v.emplace_object();

    obj["type"] = "stream9::json::pointer::error",
    obj["code"] = str::to_string(e.code());
    obj["pointer"] = e.pointer();
    obj["pos"] = e.pos();

    try {
        std::rethrow_if_nested(e);
    }
    catch (json::pointer::error const& e) {
        obj["nested_error"] = json::value_from(e);
    }
    catch (std::exception const& e) {
        obj["nested_error"] = e.what();
    }
}

} // namespace stream9::json
