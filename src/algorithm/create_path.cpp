#include <stream9/json/algorithm/create_path.hpp>

#include "../parse_array_index.hpp"

#include <stream9/json/internal_error.hpp>
#include <stream9/json/namespace.hpp>
#include <stream9/json/pointer.hpp>

#include <stream9/json/algorithm/find.hpp>
#include <stream9/json/algorithm/walk.hpp>

#include <stream9/ranges/size.hpp>
#include <stream9/ranges/views/indexed.hpp>

namespace stream9::json {

static json::value&
create_node_on_array(json::array& arr,
                     json::pointer::array_index_type const idx,
                     json::value&& v,
                     create_option const& opt)
{
    using E = pointer::errc;

    auto size = rng::ssize(arr);

    if (idx < size) {
        return arr[idx] = std::move(v);
    }
    else if (idx == size) {
        arr.push_back(std::move(v));
        return arr.back();
    }
    else {
        if (!opt.create_empty_value_on_array) {
            throw E::array_index_out_of_bound;
        }

        for (auto i = size; i < idx; ++i) {
            arr.emplace_back(nullptr);
        }

        arr.push_back(std::move(v));
        return arr.back();
    }
}

static json::value&
create_node_on_object(json::object& obj,
                      std::string_view const key,
                      json::value&& v)
{
    return obj[key] = std::move(v);
}

static json::value&
create_node_on_new_value(json::value& v1,
                         std::string_view const token,
                         json::value&& v2,
                         create_option const& opt)
{
    using E = pointer::errc;

    if (opt.regard_all_token_of_absent_path_as_object_key) {
        return create_node_on_object(
                           v1.emplace_object(), token, std::move(v2));
    }
    else {
        auto const& [ec, idx] = pointer_::parse_array_index(token, 0);

        if (ec == E::ok) {
            return create_node_on_array(
                                 v1.emplace_array(), idx, std::move(v2), opt);
        }
        else {
            return create_node_on_object(
                               v1.emplace_object(), token, std::move(v2));
        }
    }
}

static json::value&/*leaf*/
create_branch(json::value& node, json::pointer_view const& path,
              create_option const& opt)
{
    using E = pointer::errc;

    if (path.empty()) return node;

    auto* leaf = &node;

    for (auto const& [idx, token]: rng::views::indexed(path)) {
        try {
            leaf = &create_node_on_new_value(*leaf, token, nullptr, opt);
        }
        catch (E const& ec) {
            throw pointer::error { ec, path, idx };
        }
    }

    return *leaf;
}

static json::value&
splice_branch(json::value& v1, std::string_view const token,
              json::value&& v2, create_option const& opt)
{
    using E = pointer::errc;

    if (v1.is_object()) {
        return create_node_on_object(v1.get_object(), token, std::move(v2));
    }
    else if (v1.is_array()) {
        auto& arr = v1.get_array();

        auto const& [ec, idx] =
            pointer_::parse_array_index(token, rng::ssize(arr));

        if (ec == E::ok) {
            return create_node_on_array(arr, idx, std::move(v2), opt);
        }
        else {
            if (opt.overwrite_array_when_token_is_not_array_index) {
                return create_node_on_new_value(v1, token, std::move(v2), opt);
            }
            else {
                throw E::invalid_array_index;
            }
        }
    }
    else if (v1.is_null()) {
        if (opt.overwrite_null_on_the_path) {
            return create_node_on_new_value(v1, token, std::move(v2), opt);
        }
        else {
            throw pointer_::is_array_index(token) ? E::array_index_on_null
                                                  : E::object_key_on_null;
        }
    }
    else {
        if (opt.overwrite_scalar_on_the_path) {
            return create_node_on_new_value(v1, token, std::move(v2), opt);
        }
        else {
            throw pointer_::is_array_index(token) ? E::array_index_on_scalar
                                                  : E::object_key_on_scalar;
        }
    }
}

json::value&
create_path_impl(json::value& root, json::pointer_view const& path,
                 create_option const& opt)
{
    using E = pointer::errc;

    if (path.empty()) return root;

    auto const rv = json::walk(root, path);
    if (rv.ec == E::ok) {
        return rv.value;
    }

    auto const absent_path = path.slice(rv.n_step);

    assert(!absent_path.empty());

    json::value branch_root;
    auto& leaf = create_branch(branch_root, absent_path.slice(1), opt);

    try {
        auto& branch_node = splice_branch(
                           rv.value, absent_path[0], std::move(branch_root), opt);

        if (absent_path.size() == 1) {
            return branch_node; // because leaf == branch_root which is moved
        }
        else {
            return leaf;
        }
    }
    catch (E const& ec) {
        throw pointer::error { ec, absent_path, 0 };
    }
}

json::value&
create_path(json::value& root, json::pointer_view const& path,
            create_option const& opt/*= {}*/)
{
    try {
        return create_path_impl(root, path, opt);
    }
    catch (pointer::error const&) {
        throw;
    }
    catch (...) {
        json::value cxt;
        auto& obj = cxt.emplace_object();

        obj["origin"] = __PRETTY_FUNCTION__;
        obj["root"] = root;
        obj["path"] = path.to_value();
        obj["opt"] = json::value_from(opt);

        throw internal_error { std::move(cxt) };
    }
}

std::ostream&
operator<<(std::ostream& os, create_option const opt)
{
    return os << json::value_from(opt);
}

void
tag_invoke(json::value_from_tag, json::value& v, create_option const opt)
{
    auto& obj = v.emplace_object();

    obj["overwrite_null_on_the_path"] = opt.overwrite_null_on_the_path;
    obj["overwrite_scalar_on_the_path"] = opt.overwrite_scalar_on_the_path;
    obj["overwrite_array_when_token_is_not_array_index"] =
                         opt.overwrite_array_when_token_is_not_array_index;
    obj["create_empty_value_on_array"] = opt.create_empty_value_on_array;
    obj["regard_all_token_of_absent_path_as_object_key"] =
                         opt.regard_all_token_of_absent_path_as_object_key;
}

} // namespace stream9::json
