#include <stream9/json/algorithm/erase.hpp>

#include "../parse_array_index.hpp"

#include <stream9/json/namespace.hpp>

#include <stream9/json/algorithm/walk.hpp>

#include <stream9/ranges/iterator_at.hpp>
#include <stream9/ranges/size.hpp>

namespace stream9::json {

json::value&
erase(json::value& v, pointer_view const& p)
{
    using E = pointer::errc;

    if (p.empty()) {
        throw pointer::error(E::erase_with_empty_pointer, p, 0);
    }

    auto rv = json::walk(v, p.slice(0, -1));
    if (rv.ec != E::ok) {
        throw pointer::error(rv.ec, p, rv.n_step);
    }

    auto& parent = rv.value;
    auto& last_token = p.back();

    switch (parent.kind()) {
        case json::kind::object: {
            auto& obj = parent.get_object();

            if (!obj.contains(last_token)) {
                throw pointer::error(E::object_key_does_not_exist, p, rv.n_step);
            }

            obj.erase(last_token);
            break;
        }
        case json::kind::array: {
            using E = pointer::errc;

            auto& arr = parent.get_array();

            auto const& [ec, idx] =
                pointer_::parse_array_index(last_token, rng::ssize(arr));

            if (ec != E::ok) {
                throw pointer::error(ec, p, rv.n_step);
            }
            else if (idx >= rng::ssize(arr)) {
                throw pointer::error(E::array_index_out_of_bound, p, rv.n_step);
            }

            arr.erase(rng::iterator_at(arr, idx));
            break;
        }
        case json::kind::null: {
            throw pointer::error {
                pointer_::is_array_index(last_token) ? E::array_index_on_null
                                                     : E::object_key_on_null,
                p, rv.n_step
            };
        }
        default: {
            throw pointer::error {
                pointer_::is_array_index(last_token) ? E::array_index_on_scalar
                                                     : E::object_key_on_scalar,
                p, rv.n_step
            };
        }
    }

    return parent;
}

} // namespace stream9::json
