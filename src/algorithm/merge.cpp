#include <stream9/json/algorithm/merge.hpp>

#include <stream9/json/internal_error.hpp>

namespace stream9::json {

static json::value
merge_impl(json::value& target, json::value const& patch)
{
    if (!patch.is_object()) return patch;

    auto& patch_o = patch.get_object();

    if (!target.is_object()) {
        target.emplace_object();
    }

    auto& target_o = target.get_object();

    for (auto&& [name, value]: patch_o) {
        if (value.is_null()) {
            auto const it = target_o.find(name);
            if (it != target_o.end()) {
                target_o.erase(it);
            }
        }
        else {
            target_o[name] = merge_impl(target_o[name], value);
        }
    }

    return target;
}

json::value
merge(json::value const& target, json::value const& patch)
{
    try {
        auto copy = target;

        return merge_impl(copy, patch);
    }
    catch (...) {
        json::value cxt;
        auto& obj = cxt.emplace_object();

        obj["origin"] = __PRETTY_FUNCTION__;
        obj["target"] = target;
        obj["patch"] = patch;

        throw internal_error { std::move(cxt) };
    }
}

} // namespace stream9::json
