#include <stream9/json/algorithm/move.hpp>

#include "../parse_array_index.hpp"

#include <stream9/json/internal_error.hpp>

#include <stream9/json/algorithm/erase.hpp>
#include <stream9/json/algorithm/extract.hpp>
#include <stream9/json/algorithm/find.hpp>
#include <stream9/json/algorithm/insert.hpp>
#include <stream9/json/algorithm/replace.hpp>

#include <stream9/ranges/is_prefix.hpp>
#include <stream9/to_string.hpp>

namespace stream9::json {

static json::value&
do_insert(json::value& v1,
          json::pointer_view const& path,
          json::value&& v2,
          move_method const method,
          insert_option const& opt )
{
    using M = move_method;

    switch (method) {
        default:
        case M::insert:
            return json::insert(v1, path, std::move(v2), opt);
        case M::replace:
            return json::replace(v1, path, std::move(v2));
        case M::insert_or_replace:
            return json::insert_or_replace(v1, path, std::move(v2), opt);
        case M::replace_or_insert:
            return json::replace_or_insert(v1, path, std::move(v2), opt);
    }
}

json::value&
move_impl(json::value& v1,
          json::pointer_view const& src,
          json::value& v2,
          json::pointer_view const& dest,
          move_method const method/*= move_method::insert*/,
          insert_option const& opt/*= {}*/)
{
    using E = pointer::errc;

    if (&v1 == &v2) {
        if (src == dest) return json::find_or_throw(v1, src);

        if (rng::is_proper_prefix(src, dest)) {
            throw pointer::error {
                E::move_source_is_ancestor_of_destination, src, 0
            };
        }
    }

    if (src.empty()) {
        throw pointer::error {
            E::move_from_empty_pointer, src, 0
        };
    }

    if (dest.empty() && method == move_method::insert) {
        throw pointer::error {
            E::move_to_empty_pointer, dest, 0
        };
    }

    auto& src_parent_v = json::find_or_throw(v1, src.slice(0, -1));
    auto src_parent_copy_v = src_parent_v;

    auto v = json::extract(src_parent_copy_v, src.slice(-1));

    auto& dest_parent_v = json::find_or_throw(v2, dest.slice(0, -1));

    // in case v1 and v2 is identical and src and dest has same parent
    auto* const dest_parent_v_p =
        &src_parent_v == &dest_parent_v ? &src_parent_copy_v : &dest_parent_v;

    auto& result = do_insert(*dest_parent_v_p,
                               dest.slice(-1), std::move(v), method, opt);

    src_parent_v.swap(src_parent_copy_v);

    return result;
}

json::value&
move(json::value& v1,
     json::pointer_view const& src,
     json::value& v2,
     json::pointer_view const& dest,
     move_method const method/*= move_method::insert*/,
     insert_option const& opt/*= {}*/)
{
    try {
        return move_impl(v1, src, v2, dest, method, opt);
    }
    catch (pointer::error const&) {
        throw;
    }
    catch (...) {
        json::value cxt;
        auto& obj = cxt.emplace_object();

        obj["origin"] = __PRETTY_FUNCTION__;
        obj["v1"] = v1;
        obj["src"] = src.to_value();
        obj["v2"] = v2;
        obj["dest"] = dest.to_value();
        obj["method"] = str::to_string(method);
        obj["opt"] = json::value_from(opt);

        throw internal_error { std::move(cxt) };
    }
}

static auto
to_string(move_method const m)
{
    using M = move_method;

    switch (m) {
        case M::insert:
            return "insert";
        case M::replace:
            return "replace";
        case M::insert_or_replace:
            return "insert_or_replace";
        case M::replace_or_insert:
            return "replace_or_insert";
    }

    return "unknown";
}

std::ostream&
operator<<(std::ostream& os, move_method const m)
{
    return os << to_string(m);
}

} // namespace stream9::json
