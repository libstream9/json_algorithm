#include <stream9/json/algorithm/match.hpp>

#include <stream9/json/namespace.hpp>
#include <stream9/json/internal_error.hpp>
#include <stream9/json/value.hpp>

#include <algorithm>

#include <boost/container/flat_set.hpp>
#include <boost/container/small_vector.hpp>

namespace stream9::json {

static bool
match(json::value const& v1, json::value const& v2,
      json::pointer const& path, mismatch_callback, bool& stop);

static bool
ignore(json::pointer const&, json::value const&, json::value const&)
{
    return true;
}

static bool
match(json::array const& v1, json::array const& v2,
      json::pointer const& path, mismatch_callback cb, bool& stop)
{
    boost::container::small_flat_set<json::array::const_iterator, 10> skip;

    auto idx2 = 0;
    bool result = true;

    for (auto const& e2: v2) {
        auto p = path / idx2;
        ++idx2;

        bool mismatch_in_child = false;

        auto it = v1.begin();
        for (; it != v1.end(); ++it) {
            if (skip.contains(it)) continue;
            auto&& e1 = *it;

            if ((e1.is_array() && e2.is_array()) ||
                (e1.is_object() && e2.is_object()) )
            {
                if (match(e1, e2, p, cb, stop)) {
                    skip.insert(it);
                    break;
                }
                else {
                    mismatch_in_child = true;
                }
            }
            else {
                if (match(e1, e2, p, ignore, stop)) {
                    skip.insert(it);
                    break;
                }
            }
        }

        if (it == v1.end()) {
            result = false;
            if (!mismatch_in_child) {
                stop = cb(p, nullptr, e2);
            }
        }

        if (stop) break;
    }

    return result;
}

static bool
match(json::object const& v1, json::object const& v2,
      json::pointer const& path, mismatch_callback cb, bool& stop)
{
    json::pointer p = path;
    bool result = true;

    for (auto&& [k2, e2]: v2) {
        auto p = path / k2;

        auto it = v1.find(k2);
        if (it == v1.end()) {
            result = false;
            stop = cb(p, nullptr, e2);
        }
        else {
            auto&& [k1, e1] = *it;

            if (!match(e1, e2, p, cb, stop)) {
                result = false;
            }
        }

        if (stop) break;
    }

    return result;
}

static bool
match(json::value const& v1, json::value const& v2,
      json::pointer const& path, mismatch_callback cb, bool& stop)
{
    using K = json::kind;

    switch (v1.kind()) {
        case K::null:
        case K::bool_:
        case K::int64:
        case K::uint64:
        case K::double_:
        case K::string:
            if (v1 == v2) {
                return true;
            }
            break;
        case K::array:
            if (v2.is_array()) {
                return match(v1.get_array(), v2.get_array(), path, cb, stop);
            }
            break;
        case K::object:
            if (v2.is_object()) {
                return match(v1.get_object(), v2.get_object(), path, cb, stop);
            }
            break;
    }

    stop = cb(path, v1, v2);
    return false;
}

bool
match(json::value const& v1, json::value const& v2, mismatch_callback cb)
{
    try {
        json::pointer path;
        bool stop = false;

        return match(v1, v2, path, cb, stop);
    }
    catch (...) {
        throw internal_error { json::object {
            { "origin", __PRETTY_FUNCTION__ },
            { "v1", v1 },
            { "v2", v2 },
        }};
    }
}

match_result
match(json::value const& v1, json::value const& v2)
{
    try {
        match_result result;
        json::pointer path;
        json::pointer mismatch;
        bool stop = false;

        result.match = match(v1, v2, path,
            [&](auto&& p, auto&&, auto&&) {
                result.mismatch = p;
                return true;
            }, stop);

        return result;
    }
    catch (...) {
        throw internal_error { json::object {
            { "origin", __PRETTY_FUNCTION__ },
            { "v1", v1 },
            { "v2", v2 },
        }};
    }
}

} // namespace stream9::json
