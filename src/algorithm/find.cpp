#include <stream9/json/algorithm/find.hpp>

#include <stream9/json/namespace.hpp>

#include <stream9/json/algorithm/walk.hpp>

namespace stream9::json {

json::value*
find(json::value& v, pointer_view const& p) noexcept
{
    using E = pointer::errc;

    auto rv = json::walk(v, p);
    if (rv.ec != E::ok) {
        return nullptr;
    }
    else {
        return &rv.value;
    }
}

json::value&
find_or_throw(json::value& v, pointer_view const& p)
{
    using E = pointer::errc;

    auto rv1 = json::walk(v, p);
    if (rv1.ec == E::ok) {
        return rv1.value;
    }
    else {
        throw pointer::error(rv1.ec, p, rv1.n_step);
    }
}

} // namespace stream9::json
