#include <stream9/json/algorithm/walk.hpp>

#include "../parse_array_index.hpp"

#include <stream9/json/namespace.hpp>

#include <ranges>
#include <string_view>

namespace stream9::json {

struct step_result {
    pointer::errc ec = pointer::errc::ok;
    json::value* o_value = nullptr;
};

static step_result
step(json::value& v, std::string_view const token)
{
    using E = pointer::errc;

    switch (v.kind()) {
        case json::kind::object: {
            auto& obj = v.get_object();
            auto it = obj.find(token);
            if (it == obj.end()) {
                return {
                    .ec = E::object_key_does_not_exist,
                };
            }
            else {
                return { .o_value = &(*it).second };
            }
            break;
        }
        case json::kind::array: {
            auto& arr = v.get_array();
            auto [ec, idx] =
                pointer_::parse_array_index(token, rng::ssize(arr));
            if (ec == E::ok) {
                if (idx >= rng::ssize(arr)) {
                    return { .ec = E::array_index_out_of_bound };
                }
                else {
                    return { .o_value = &(arr)[idx] };
                }
            }
            else {
                return { .ec = ec };
            }
            break;
        }
        case json::kind::null:
            return {
                .ec = pointer_::is_array_index(token) ? E::array_index_on_null
                                                      : E::object_key_on_null
            };
        default:
            return {
                .ec = pointer_::is_array_index(token) ? E::array_index_on_scalar
                                                      : E::object_key_on_scalar
            };
    }
}

walk_result
walk(json::value& v, json::pointer_view const& p) noexcept
{
    using E = pointer::errc;

    if (p.empty()) {
        return {
            .value = v,
            .n_step = 0,
        };
    }

    auto* cur = &v;

    pointer_view::difference_type idx = 0;
    for (; idx < p.ssize(); ++idx) {
        auto const& token = p[idx];

        auto const r = step(*cur, token);

        if (r.ec != E::ok) {
            return { .ec = r.ec, .value = *cur, .n_step = idx };
        }
        else {
            cur = r.o_value;
        }
    }

    return { .value = *cur, .n_step = idx };
}

} // namespace stream9::json
