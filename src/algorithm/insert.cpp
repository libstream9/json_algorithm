#include <stream9/json/algorithm/insert.hpp>

#include "insert_p.hpp"

#include "../parse_array_index.hpp"

#include <stream9/json/internal_error.hpp>
#include <stream9/json/namespace.hpp>

#include <stream9/json/algorithm/create_path.hpp>
#include <stream9/json/algorithm/find.hpp>

#include <ranges>
#include <string_view>

namespace stream9::json {

template<typename T>
json::value&
insert_value(json::value& v1, std::string_view const token, T&& v2,
             insert_option const& opt = {},
             bool replace_on_duplicate = false)
{
    using E = pointer::errc;

    switch (v1.kind()) {
        case json::kind::null:
            return pointer_::insert_to_null(
                                         v1, token, std::forward<T>(v2), opt);
        case json::kind::object: {
            auto& obj = v1.get_object();

            if (replace_on_duplicate) {
                auto const& [it, _] =
                    obj.insert_or_assign(token, std::forward<T>(v2));

                return (*it).second;
            }
            else {
                auto const& [it, ok] = obj.emplace(token, std::forward<T>(v2));
                if (!ok) {
                    throw E::object_key_already_exist;
                }

                return (*it).second;
            }
        }
        case json::kind::array: {
            if (opt.regard_last_token_of_path_as_object_key) {
                throw E::invalid_array_index;
            }

            auto& arr = v1.get_array();

            auto const& [ec, idx] =
                pointer_::parse_array_index(token, rng::ssize(arr));
            if (ec != E::ok) {
                throw ec;
            }
            else if (idx > rng::ssize(arr)) {
                if (opt.create_empty_value_on_array) {
                    auto& arr = v1.emplace_array();

                    pointer_::fill_array(arr, 0, idx);
                    arr.push_back(std::forward<T>(v2));

                    return arr.back();
                }
                else {
                    throw E::array_index_out_of_bound;
                }
            }
            else {
                auto const pos = arr.begin() + idx;

                auto const it = arr.insert(pos, std::forward<T>(v2));
                return *it;
            }
        }
        default:
            throw pointer_::is_array_index(token) ? E::array_index_on_scalar
                                                  : E::object_key_on_scalar;
    }
}

/*
 * insert
 */
template<typename T>
json::value&
insert_impl(json::value& v1, pointer_view const& p, T&& v2,
            insert_option const& opt,
            bool replace_on_duplicate = false)
{
    using E = pointer::errc;

    if (p.empty() && !replace_on_duplicate) {
        throw pointer::error(E::insert_with_empty_pointer, p, 0);
    }

    auto& parent_v = json::find_or_throw(v1, p.slice(0, -1));

    try {
        return insert_value(parent_v, p.back(), std::forward<T>(v2), opt,
                            replace_on_duplicate);
    }
    catch (E const& ec) {
        throw pointer::error(ec, p, -1);
    }
}

json::value&
insert(json::value& v1, pointer_view const& p, json::value const& v2,
       insert_option const& opt/*= {}*/)
{
    try {
        return insert_impl(v1, p, v2, opt);
    }
    catch (pointer::error const&) {
        throw;
    }
    catch (...) {
        json::value cxt;
        auto& obj = cxt.emplace_object();

        obj["origin"] = __PRETTY_FUNCTION__;
        obj["v1"] = v1;
        obj["path"] = p.to_value();
        obj["v2"] = v2;
        obj["opt"] = json::value_from(opt);

        throw internal_error { std::move(cxt) };
    }
}

json::value&
insert(json::value& v1, pointer_view const& p, json::value&& v2,
       insert_option const& opt/*= {}*/)
{
    try {
        return insert_impl(v1, p, std::move(v2), opt);
    }
    catch (pointer::error const&) {
        throw;
    }
    catch (...) {
        json::value cxt;
        auto& obj = cxt.emplace_object();

        obj["origin"] = __PRETTY_FUNCTION__;
        obj["v1"] = v1;
        obj["path"] = p.to_value();
        obj["v2"] = v2;
        obj["opt"] = json::value_from(opt);

        throw internal_error { std::move(cxt) };
    }
}

/*
 * insert_or_replace
 */
template<typename T>
json::value&
insert_or_replace_impl(json::value& v1, pointer_view const& p, T&& v2,
            insert_option const& opt)
{
    using E = pointer::errc;

    if (p.empty()) {
        return v1 = std::forward<T>(v2);
    }

    auto& parent_v = json::find_or_throw(v1, p.slice(0, -1));

    try {
        return insert_value(parent_v, p.back(), std::forward<T>(v2), opt, true);
    }
    catch (E const& ec) {
        throw pointer::error(ec, p, -1);
    }
}

json::value&
insert_or_replace(json::value& v1,
                  pointer_view const& p, json::value const& v2,
                  insert_option const& opt/*= {}*/)
{
    try {
        return insert_or_replace_impl(v1, p, v2, opt);
    }
    catch (pointer::error const&) {
        throw;
    }
    catch (...) {
        json::value cxt;
        auto& obj = cxt.emplace_object();

        obj["origin"] = __PRETTY_FUNCTION__;
        obj["v1"] = v1;
        obj["path"] = p.to_value();
        obj["v2"] = v2;
        obj["opt"] = json::value_from(opt);

        throw internal_error { std::move(cxt) };
    }
}

json::value&
insert_or_replace(json::value& v1,
                  pointer_view const& p, json::value&& v2,
                  insert_option const& opt/*= {}*/)
{
    try {
        return insert_or_replace_impl(v1, p, std::move(v2), opt);
    }
    catch (pointer::error const&) {
        throw;
    }
    catch (...) {
        json::value cxt;
        auto& obj = cxt.emplace_object();

        obj["origin"] = __PRETTY_FUNCTION__;
        obj["v1"] = v1;
        obj["path"] = p.to_value();
        obj["v2"] = v2;
        obj["opt"] = json::value_from(opt);

        throw internal_error { std::move(cxt) };
    }
}

void
tag_invoke(json::value_from_tag, json::value& v, insert_option const& opt)
{
    auto& obj = v.emplace_object();
    obj["create_new_value_on_null"] = opt.create_new_value_on_null;
    obj["create_empty_value_on_array"] = opt.create_empty_value_on_array;
    obj["regard_last_token_of_path_as_object_key"] =
                             opt.regard_last_token_of_path_as_object_key;
}

} // namespace stream9::json
