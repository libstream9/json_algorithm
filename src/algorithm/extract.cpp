#include <stream9/json/algorithm/extract.hpp>

#include <stream9/json/algorithm/find.hpp>

#include "../parse_array_index.hpp"

#include <stream9/ranges/iterator_at.hpp>
#include <stream9/ranges/size.hpp>

namespace stream9::json {

template<typename T>
json::value
extract_impl(T&& v, json::pointer_view const& path )
{
    using E = pointer::errc;

    if (path.empty()) {
        throw pointer::error {
            E::extract_with_empty_pointer, path, 0
        };
    }

    auto& parent = json::find_or_throw(v, path.slice(0, -1));
    auto const& token = path.back();

    if (parent.is_object()) {
        auto& obj = parent.get_object();

        auto const it = obj.find(token);
        if (it == obj.end()) {
            throw pointer::error {
                E::object_key_does_not_exist,
                path, -1
            };
        }
        else {
            auto result = std::move((*it).second);
            obj.erase(it);

            return result;
        }
    }
    else if (parent.is_array()) {
        auto& arr = parent.get_array();

        auto const [ec, idx] =
            pointer_::parse_array_index(token, rng::ssize(arr));
        if (ec != E::ok) {
            throw pointer::error { ec, path, -1 };
        }
        else if (idx >= rng::ssize(arr)) {
            throw pointer::error {
                E::array_index_out_of_bound,
                path, -1
            };
        }
        else {
            auto it = rng::iterator_at(arr, idx);
            auto result = std::move(*it);
            arr.erase(it);

            return result;
        }
    }
    else if (parent.is_null()) {
        throw pointer::error {
            pointer_::is_array_index(token) ? E::array_index_on_null
                                            : E::object_key_on_null,
            path, -1
        };
    }
    else {
        throw pointer::error {
            pointer_::is_array_index(token) ? E::array_index_on_scalar
                                            : E::object_key_on_scalar,
            path, -1
        };
    }
}

json::value
extract(json::value& v, json::pointer_view const& path )
{
    return extract_impl(v, path);
}

json::value
extract(json::value&& v, json::pointer_view const& path )
{
    return extract_impl(std::move(v), path);
}

} // namespace stream9::json
