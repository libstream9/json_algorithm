#include <stream9/json/algorithm/diff.hpp>

#include <stream9/json/namespace.hpp>
#include <stream9/json/pointer.hpp>

#include <boost/container/flat_set.hpp>

#include <ranges>

#include <stream9/ranges/boundary.hpp>
#include <stream9/myers_diff.hpp>

namespace stream9::json {

// diff(null, null) ->
//
// diff(1, null) -> replace("", null)
// diff(null, 1) -> replace("", null)
// diff(1, "foo") -> replace("", "foo")
//
// diff(null, [1, 2]) -> replace("", [1, 2])
// diff([1, 2], null) -> replace("", null)
// diff(true, [1, 2]) -> replace("", [1, 2)
//
// diff(null, {"foo":1}) -> replace("", {"foo":1})
// diff({}, {"foo":1}) -> add("/foo", 1)
// diff({"foo":1}, {}) -> remove("/foo")
// diff({"foo":1}, {"foo":2}) -> replace("/foo", 2)
// diff({"foo":1}, {"bar":1}) -> move("/foo", "/bar)
// diff({"foo":1}, {"bar":2}) -> remove("/foo")
//                               add("/bar", 2)
//
// diff({"foo":{}}, {"foo":{"abc":1}}) -> add("/foo/abc", 1)
// diff({"foo",{"abc":1}}, {"foo":{}}) -> remove("/foo/abc")
// diff({"foo",{"abc":1}}, {"foo":{"abc":2}}) -> replace("/foo/abc", 2)
// diff({"foo",{"abc":1}}, {"foo":{"xyz":1}}) -> move("/foo/abc", "/foo/xyz"))
// diff({"foo",{"abc":1}}, {"foo":{"xyz":2}}) -> remove("/foo/abc")
//                                               add("/foo/xyz", 2)
//
// diff([], [1]) -> add("/0", 1)
// diff([1], []) -> remove("/0")
// diff([1], [2]) -> replace("/0", 2)
//
// diff([1], [1,2]) -> add("/1", 2)
// diff([1,2], [1]) -> remove("/1")
// diff([1,2], [1,3]) -> replace("/1", 3)
// diff([1,2], [2,1]) -> move("/0", "/1")
// diff([1,2], [3,4]) -> replace("/0", 3)
//                       replace("/1", 4)
//
// diff([1,2], [2,[1]]) -> replace("/0", 2)
//                         replace("/1", [1])
// diff([[1],[2]], [[1],[3]]) -> replace("/1", [3])

static void
diff_value(pointer const&,
           json::value const& v1, json::value const v2,
           diff_event_handler&);

static void
diff_array(pointer const&,
           json::value const& v1, json::value const& v2,
           diff_event_handler&);

static auto
sorted_keys(json::object const& o)
{
    boost::container::flat_set<std::string_view> result;

    for (auto&& [key, _]: o) {
        result.emplace(key);
    }

    return result;
}

struct object_edit_event {
    enum class event_type { add, remove, replace, move } type;
    std::string_view key1 {};
    std::string_view key2 {};
    json::value const* value1 = nullptr;
    json::value const* value2 = nullptr;
};

class object_edit_sequence
{
    using event = object_edit_event;

public:
    void add(std::string_view const k, json::value const& v)
    {
        using et = event::event_type;

        m_edits.push_back({
            .type = et::add,
            .key1 = k,
            .value1 = &v,
        });
    }

    void remove(std::string_view const k, json::value const& v)
    {
        using et = event::event_type;

        m_edits.push_back({
            .type = et::remove,
            .key1 = k,
            .value1 = &v,
        });
    }

    void replace(std::string_view const k,
                 json::value const& v1, json::value const& v2)
    {
        using et = event::event_type;

        m_edits.push_back({
            .type = et::replace,
            .key1 = k,
            .value1 = &v1,
            .value2 = &v2,
        });
    }

    void notify(pointer const& base, diff_event_handler& h)
    {
        using et = event::event_type;

        synthesize_move_event();

        for (auto const& e: m_edits) {
            switch (e.type) {
                case et::add:
                    h.add(base / e.key1, *e.value1);
                    break;
                case et::remove:
                    h.remove(base / e.key1, *e.value1);
                    break;
                case et::replace:
                    diff_value(base / e.key1, *e.value1, *e.value2, h);
                    break;
                case et::move:
                    h.move(base / e.key1, *e.value1, base / e.key2);
                    break;
            }
        }
    }

private:
    void synthesize_move_event()
    {
        auto&& [it1, end] = rng::boundary(m_edits);
        for (; it1 != end;) {
            auto& e1 = *it1;

            if (e1.type == event::event_type::add) {
                // find corresponding remove event
                auto it2 = std::ranges::find_if(m_edits,
                    [&](auto&& e2) {
                        return e2.type == event::event_type::remove
                            && *e2.value1 == *e1.value1;
                    } );

                if (it2 != m_edits.end()) {
                    // transform into move event
                    it2->type = event::event_type::move;
                    it2->key2 = e1.key1;

                    it1 = m_edits.erase(it1);
                    continue;
                }
            }

            ++it1;
        }
    }

private:
    std::vector<object_edit_event> m_edits;
};

static void
diff_object(pointer const& path,
            json::value const& v1, json::value const& v2,
            diff_event_handler& h)
{
    assert(v1.is_object());
    assert(v2.is_object());

    auto const& o1 = v1.get_object();
    auto const& o2 = v2.get_object();

    auto keys1 = sorted_keys(o1);
    auto keys2 = sorted_keys(o2);

    auto [it1, end1] = rng::boundary(keys1);
    auto [it2, end2] = rng::boundary(keys2);

    object_edit_sequence edits;

    while (it1 != end1 || it2 != end2) {
        if (it1 == end1) {
            assert(it2 != end2);

            auto const& k2 = *it2;
            edits.add(k2, o2.at(k2));

            ++it2;
        }
        else if (it2 == end2) {
            assert(it1 != end1);

            auto const& k1 = *it1;
            edits.remove(k1, o1.at(k1));

            ++it1;
        }
        else {
            assert(it1 != end1);
            assert(it2 != end2);

            auto const& k1 = *it1;
            auto const& k2 = *it2;

            if (k1 != k2) {
                edits.remove(k1, o1.at(k1));

                ++it1;
            }
            else {
                auto const& v1 = o1.at(k1);
                auto const& v2 = o2.at(k2);

                edits.replace(k1, v1, v2);

                ++it1, ++it2;
            }
        }
    }

    edits.notify(path, h);
}

struct array_edit_event
{
    enum class event_type { add, remove, replace, move, } type;
    myers_diff::uindex_t index1 = 0;
    myers_diff::uindex_t index2 = 0;
    json::value const* value1 = nullptr;
    json::value const* value2 = nullptr;
};

class array_edit_sequence
{
public:
    using index_t = myers_diff::uindex_t;
    using event = array_edit_event;

public:
    void add(index_t const i, json::value const& v)
    {
        m_edits.push_back({
            .type = event::event_type::add,
            .index1 = i,
            .value1 = &v
        });
    }

    void remove(index_t const i, json::value const& v)
    {
        m_edits.push_back({
            .type = event::event_type::remove,
            .index1 = i,
            .value1 = &v
        });
    }

    void notify(pointer const& base, diff_event_handler& h)
    {
        using et = event::event_type;

        synthesize_replace_event();
        synthesize_move_event();

        for (auto const& e: m_edits) {
            switch (e.type) {
                case et::add:
                    h.add(base / e.index1, *e.value1);
                    break;
                case et::remove:
                    h.remove(base / e.index1, *e.value1);
                    break;
                case et::replace:
                    diff_value(base / e.index1, *e.value1, *e.value2, h);
                    break;
                case et::move:
                    h.move(base / e.index1, *e.value1, base / e.index2);
            }
        }
    }

private:
    void synthesize_replace_event()
    {
        using et = event::event_type;

        for (auto cur = m_edits.begin(); cur != m_edits.end(); ++cur) {
            auto next = cur + 1;
            if (next == m_edits.end()) break;

            if (cur->type == et::add &&
                next->type == et::remove &&
                next->index1 == cur->index1 + 1)
            {
                next->type = et::replace;
                next->index1 = cur->index1;
                next->value2 = cur->value1;

                cur = m_edits.erase(cur);
            }
            else if (cur->type == et::remove &&
                     next->type == et::add &&
                     next->index1 == cur->index1)
            {
                cur->type = et::replace;
                cur->value2 = next->value1;

                m_edits.erase(next);
            }
        }
    }

    void synthesize_move_event()
    {
        using et = event::event_type;

        for (auto it = m_edits.begin(); it != m_edits.end(); ++it) {
            switch (it->type) {
                case et::remove: {
                    auto it2 = rng::find_if(it + 1, m_edits.end(),
                        [&](auto&& e) {
                            return e.type == et::add
                                && *e.value1 == *it->value1;
                        } );

                    if (it2 != m_edits.end()) {
                        it->type = et::move;
                        it->index2 = it2->index1;

                        m_edits.erase(it2);
                    }
                    break;
                }
                case et::add: {
                    index_t offset = 1;
                    for (auto it2 = it + 1; it2 != m_edits.end(); ++it2) {
                        if (it2->type == et::add) {
                            ++offset;
                        }
                        else if (it2->type == et::remove &&
                                 *it2->value1 == *it->value1)
                        {
                            if (it2->index1 >= offset) [[likely]] {
                                auto const from = it2->index1 - offset;
                                auto const to = it->index1;

                                it->type = et::move;
                                it->index1 = from;
                                it->index2 = to;

                                m_edits.erase(it2);
                                break;
                            }
                        }
                    }
                    break;
                }
                default:
                    break;
            }
        }
    }

private:
    std::vector<array_edit_event> m_edits;
};

static void
diff_array(pointer const& path,
           json::value const& v1, json::value const& v2,
           diff_event_handler& h)
{
    assert(v1.is_array());
    assert(v2.is_array());

    auto const& a1 = v1.get_array();
    auto const& a2 = v2.get_array();

    array_edit_sequence edits;

    struct handler : myers_diff::event_handler {
        using uindex_t = myers_diff::uindex_t;

        array_edit_sequence& edits;
        pointer const& path;
        json::array const& a1;
        json::array const& a2;
        uindex_t current = 0;

        void unchanged(uindex_t/*x*/, uindex_t/*y*/)  override
        {
            ++current;
        }

        void deleted(uindex_t const i1) override
        {
            edits.remove(current, a1.at(i1));
        }

        void inserted(uindex_t const i2) override
        {
            edits.add(current, a2.at(i2));

            ++current;
        }

        handler(array_edit_sequence& e, pointer const& p,
                json::array const& a1_, json::array const& a2_)
            : edits { e }
            , path { p }
            , a1 { a1_ }
            , a2 { a2_ }
        {}
    };

    handler mh { edits, path, a1, a2 };

    myers_diff::diff(a1, a2, mh);

    edits.notify(path, h);
}

static void
diff_value(pointer const& path,
           json::value const& v1, json::value const v2,
           diff_event_handler& h)
{
    auto const kind1 = v1.kind();
    auto const kind2 = v2.kind();

    switch (kind1) {
        case json::kind::object:
            if (kind2 == json::kind::object) {
                diff_object(path, v1, v2, h);
            }
            else {
                h.replace(path, v1, v2);
            }
            break;
        case json::kind::array:
            if (kind2 == json::kind::array) {
                diff_array(path, v1, v2, h);
            }
            else {
                h.replace(path, v1, v2);
            }
            break;
        default:
            if (v2 != v1) {
                h.replace(path, v1, v2);
            }
    }
}

void
diff(json::value const& v1, json::value const& v2,
     diff_event_handler& h)
{
    diff_value({}, v1, v2, h);
}

} // namespace stream9::json
