#include <stream9/json/algorithm/includes.hpp>

#include <stream9/json/namespace.hpp>
#include <stream9/json/internal_error.hpp>

#include <algorithm>

#include <boost/container/flat_set.hpp>
#include <boost/container/small_vector.hpp>

namespace stream9::json {

static bool
includes(json::array const& v1, json::array const& v2)
{
    boost::container::small_flat_set<json::array::const_iterator, 10> skip;

    return std::ranges::all_of(v2, [&](auto& e2) {
        for (auto it = v1.begin(); it != v1.end(); ++it) {
            if (skip.contains(it)) continue;

            if (includes(*it, e2)) {
                skip.insert(it);
                return true;
            }
        }

        return false;
    });
}

static bool
includes(json::object const& v1, json::object const& v2)
{
    return std::ranges::all_of(v2, [&](auto&& p2) {
        auto& [k2, e2] = p2;

        auto it = v1.find(k2);
        return it != v1.end() && includes((*it).second, e2);
    });
}

bool
includes(json::value const& v1, json::value const& v2)
{
    try {
        using K = json::kind;
        switch (v1.kind()) {
            case K::null:
            case K::bool_:
            case K::int64:
            case K::uint64:
            case K::double_:
            case K::string:
                return v1 == v2;
            case K::array:
                return v2.is_array() && includes(v1.get_array(), v2.get_array());
            case K::object:
                return v2.is_object() && includes(v1.get_object(), v2.get_object());
        }

        return false;
    }
    catch (...) {
        throw internal_error { json::object {
            { "origin", __PRETTY_FUNCTION__ },
            { "v1", v1 },
            { "v2", v2 },
        }};
    }
}

} // namespace stream9::json
