#include <stream9/json/algorithm/replace.hpp>

#include "insert_p.hpp"
#include "../parse_array_index.hpp"

#include <stream9/json/pointer_view.hpp>
#include <stream9/json/internal_error.hpp>

#include <stream9/json/algorithm/find.hpp>

#include <stream9/ranges/last_index.hpp>
#include <stream9/ranges/size.hpp>

#include <utility>

namespace stream9::json {

namespace {

template<typename T>
json::value&
replace_impl(json::value& v1, pointer_view const& p, T&& v2)
{
    auto& target = json::find_or_throw(v1, p);

    return target = std::forward<T>(v2);
}

template<typename T>
json::value&
replace_or_insert_impl(json::value& v1, pointer_view const& p, T&& v2,
                       insert_option const& opt)
{
    using E = pointer::errc;

    if (p.empty()) {
        return v1 = std::forward<T>(v2);
    }

    auto& parent_v = json::find_or_throw(v1, p.slice(0, -1));
    auto const& last_token = p.back();

    switch (parent_v.kind()) {
        case json::kind::null:
            return pointer_::insert_to_null(
                                    v1, last_token, std::forward<T>(v2), opt);
        case json::kind::object: {
            auto& obj = v1.get_object();

            return obj[last_token] = std::forward<T>(v2);
        }
        case json::kind::array: {
            if (opt.regard_last_token_of_path_as_object_key) {
                throw E::invalid_array_index;
            }

            auto& arr = v1.get_array();
            auto const size = rng::ssize(arr);

            auto const& [ec, idx] = pointer_::parse_array_index(last_token, size);
            if (ec != E::ok) {
                throw pointer::error {
                    ec, p, rng::last_index(p)
                };
            }
            else if (idx > rng::ssize(arr)) {
                throw pointer::error {
                    E::array_index_out_of_bound,
                    p, rng::last_index(p)
                };
            }
            else if (idx == size) {
                arr.push_back(std::forward<T>(v2));
                return arr.back();
            }
            else {
                return arr[idx] = std::forward<T>(v2);
            }
        }
        default: {
            throw pointer::error {
                pointer_::is_array_index(last_token) ? E::array_index_on_scalar
                                                     : E::object_key_on_scalar,
                p, rng::last_index(p)
            };
        }
    }
}

} // anonymous namespace

/*
 * replace
 */
json::value&
replace(json::value& v1, pointer_view const& p, json::value const& v2)
{
    try {
        return replace_impl(v1, p, v2);
    }
    catch (pointer::error const&) {
        throw;
    }
    catch (...) {
        json::value cxt;
        auto& obj = cxt.emplace_object();

        obj["origin"] = __PRETTY_FUNCTION__;
        obj["v1"] = v1;
        obj["path"] = p.to_value();
        obj["v2"] = v2;

        throw internal_error { std::move(cxt) };
    }
}

json::value&
replace(json::value& v1, pointer_view const& p, json::value&& v2)
{
    try {
        return replace_impl(v1, p, std::move(v2));
    }
    catch (pointer::error const&) {
        throw;
    }
    catch (...) {
        json::value cxt;
        auto& obj = cxt.emplace_object();

        obj["origin"] = __PRETTY_FUNCTION__;
        obj["v1"] = v1;
        obj["path"] = p.to_value();
        obj["v2"] = v2;

        throw internal_error { std::move(cxt) };
    }
}

/*
 * replace_or_insert
 */
json::value&
replace_or_insert(json::value& v1, pointer_view const& p,
                  json::value const& v2, insert_option const& opt/*= {}*/)
{
    try {
        return replace_or_insert_impl(v1, p, v2, opt);
    }
    catch (pointer::error const&) {
        throw;
    }
    catch (...) {
        json::value cxt;
        auto& obj = cxt.emplace_object();

        obj["origin"] = __PRETTY_FUNCTION__;
        obj["v1"] = v1;
        obj["path"] = p.to_value();
        obj["v2"] = v2;
        obj["opt"] = json::value_from(opt);

        throw internal_error { std::move(cxt) };
    }
}

json::value&
replace_or_insert(json::value& v1, pointer_view const& p,
                  json::value&& v2, insert_option const& opt/*= {}*/)
{
    try {
        return replace_or_insert_impl(v1, p, std::move(v2), opt);
    }
    catch (pointer::error const&) {
        throw;
    }
    catch (...) {
        json::value cxt;
        auto& obj = cxt.emplace_object();

        obj["origin"] = __PRETTY_FUNCTION__;
        obj["v1"] = v1;
        obj["path"] = p.to_value();
        obj["v2"] = v2;
        obj["opt"] = json::value_from(opt);

        throw internal_error { std::move(cxt) };
    }
}

} // namespace stream9::json
