#ifndef STREAM9_JSON_ALGORITHM_INSERT_P_HPP
#define STREAM9_JSON_ALGORITHM_INSERT_P_HPP

#include "../parse_array_index.hpp"

#include <stream9/json/value.hpp>

#include <stream9/json/algorithm/insert.hpp>

#include <string_view>

namespace stream9::json::pointer_ {

inline void
fill_array(json::array& arr,
           json::pointer::array_index_type const from,
           json::pointer::array_index_type const until)
{
    for (auto i = from; i < until; ++i) {
        arr.push_back(nullptr);
    }
}

template<typename T>
json::value&
insert_to_null(json::value& v1,
               std::string_view const token,
               T&& v2,
               insert_option const& opt = {} )
{
    using E = pointer::errc;

    auto const& [ec, idx] = pointer_::parse_array_index(token, 0);

    if (!opt.create_new_value_on_null) {
        throw ec == E::ok ? E::array_index_on_null
                          : E::object_key_on_null;
    }

    if (ec != E::ok || opt.regard_last_token_of_path_as_object_key) {
        auto& obj = v1.emplace_object();

        return obj[token] = std::forward<T>(v2);
    }
    else if (idx == 0) {
        auto& arr = v1.emplace_array();

        arr.push_back(std::forward<T>(v2));

        return arr.back();
    }
    else if (opt.create_empty_value_on_array) {
        auto& arr = v1.emplace_array();

        pointer_::fill_array(arr, 0, idx);
        arr.push_back(std::forward<T>(v2));

        return arr.back();
    }
    else {
        throw E::array_index_out_of_bound;
    }
}

} // namespace stream9::json::pointer_

#endif // STREAM9_JSON_ALGORITHM_INSERT_P_HPP
