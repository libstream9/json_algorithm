#ifndef STREAM9_JSON_POINTER_PARSE_ARRAY_INDEX_HPP
#define STREAM9_JSON_POINTER_PARSE_ARRAY_INDEX_HPP

#include <stream9/json/pointer.hpp>

#include <string_view>

namespace stream9::json::pointer_ {

struct parse_array_index_result {
    pointer::errc ec {};
    pointer::array_index_type index = 0;
};

parse_array_index_result
    parse_array_index(std::string_view token,
                      pointer::array_index_type past_last_index);

bool is_array_index(std::string_view token);

} // namespace stream9::json::pointer_

#endif // STREAM9_JSON_POINTER_PARSE_ARRAY_INDEX_HPP
