#include "parse_array_index.hpp"

#include <charconv>

namespace stream9::json::pointer_ {

bool
is_array_index(std::string_view const token)
{
    using E = pointer::errc;

    return parse_array_index(token, 0).ec == E::ok;
}

parse_array_index_result
parse_array_index(std::string_view const token,
                  pointer::array_index_type const past_last_index)
{
    using E = pointer::errc;

    if (token.empty()) {
        return { .ec = E::invalid_array_index };
    }

    if (token == "-") {
        return { .index = past_last_index };
    }

    pointer::array_index_type::value_type result = 0;
    auto const& [ ptr, e ] =
        std::from_chars(token.begin(), token.end(), result);

    if (e != std::errc() || ptr != token.end()) {
        return { .ec = E::invalid_array_index };
    }
    else if (token.size() > 1 && token.front() == '0') {
        return { .ec = E::array_index_has_leading_zero };
    }

    try {
        return { .index = result };
    }
    catch (std::runtime_error const&) {
        return { .ec = E::array_index_out_of_bound };
    }
}

} // namespace stream9::json::pointer_
