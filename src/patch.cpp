#include <stream9/json/patch.hpp>

#include <stream9/json/namespace.hpp>
#include <stream9/json/parse.hpp>
#include <stream9/json/pointer_view.hpp>

#include <stream9/json/algorithm/diff.hpp>
#include <stream9/json/algorithm/erase.hpp>
#include <stream9/json/algorithm/find.hpp>
#include <stream9/json/algorithm/insert.hpp>
#include <stream9/json/algorithm/move.hpp>
#include <stream9/json/algorithm/replace.hpp>

#include <stream9/ranges/is_prefix.hpp>
#include <stream9/to_string.hpp>

namespace stream9::json {

namespace {

class patch_generator : public diff_event_handler
{
public:
    patch_generator(patch& p)
        : m_patch { p }
    {}

    void add(pointer const& path, json::value const& v) override
    {
        m_patch.add(path, v);
    }

    void remove(pointer const& path, json::value const&) override
    {
        m_patch.remove(path);
    }

    void replace(pointer const& path,
                 json::value const& from, json::value const& to) override
    {
        m_patch.test(path, from);
        m_patch.replace(path, to);
    }

    void move(pointer const& from,
              json::value const& v, pointer const& to)
    {
        m_patch.test(from, v);
        m_patch.move(from, to);
    }

private:
    patch& m_patch;
};

static std::string_view
validate_op_member(json::value const& operation)
{
    using E = patch::errc;

    assert(operation.is_object());

    auto& op = operation.get_object();

    auto const it = op.find("op");
    if (it == op.end()) {
        throw patch::error {
            E::operation_does_not_contain_op_member,
            operation
        };
    }

    auto const& o = (*it).second;
    if (!o.is_string()) {
        throw patch::error {
            E::op_member_has_to_be_string,
            operation
        };
    }

    return o.get_string();
}

static json::pointer
validate_location_member(json::value const& op, std::string_view const name)
{
    using E = patch::errc;

    assert(op.is_object());

    auto& obj = op.get_object();

    auto const it = obj.find(name);
    if (it == obj.end()) {
        throw patch::error {
            name == "from" ? E::operation_does_not_contain_from_member
                           : E::operation_does_not_contain_path_member,
            op
        };
    }

    auto& loc_v = (*it).second;
    if (!loc_v.is_string()) {
        throw patch::error {
            name == "from" ? E::from_member_has_to_be_string
                           : E::path_member_has_to_be_string,
            op
        };
    }

    try {
        return loc_v.get_string();
    }
    catch (json::pointer::error const& e) {
        throw patch::error {
            name == "from" ? E::from_member_is_not_proper_json_pointer
                           : E::path_member_is_not_proper_json_pointer,
            op,
            std::current_exception()
        };
    }
}

static void
validate_value_member(json::value const& op)
{
    using E = patch::errc;

    assert(op.is_object());

    auto& obj = op.get_object();

    auto const it = obj.find("value");
    if (it == obj.end()) {
        throw patch::error {
            E::operation_does_not_contain_value_member,
            op
        };
    }
}

static void
validate(json::value const& v)
{
    using E = patch::errc;

    if (!v.is_array()) {
        throw patch::error {
            E::patch_document_has_to_be_array,
            v
        };
    }

    auto& arr = v.get_array();

    for (auto const& op: arr) {
        if (!op.is_object()) {
            throw patch::error {
                E::operation_is_not_object,
                op
            };
        }

        auto const& op_type = validate_op_member(op);
        auto const& path = validate_location_member(op, "path");

        if (op_type == "add") {
            validate_value_member(op);
        }
        else if (op_type == "remove") {
            if (path.empty()) {
                throw patch::error {
                    E::empty_path_on_remove_operation,
                    op
                };
            }
        }
        else if (op_type == "replace") {
            validate_value_member(op);
        }
        else if (op_type == "move") {
            auto const& from = validate_location_member(op, "from");

            if (rng::is_proper_prefix(from, path)) {
                throw patch::error {
                    E::from_location_can_not_be_proper_prefix_of_path_location,
                    op
                };
            }
        }
        else if (op_type == "copy") {
            validate_location_member(op, "from");
        }
        else if (op_type == "test") {
            validate_value_member(op);
        }
        else {
            // RFC6902 section 4 says, "Members that are not
            // explicitly defined for the operation in question MUST be
            // ignored (i.e., the operation will complete as if the undefined
            // member did not appear in the object)."
            continue;
        }
    }
}

static void
apply_add(json::value& v, json::value const&,
          json::pointer_view const& path, json::value const& value)
{
    json::insert_or_replace(v, path, value, {
        .create_new_value_on_null = false
    });
}

static void
apply_remove(json::value& v, json::pointer_view const& path)
{
    json::erase(v, path);
}

static void
apply_replace(json::value& v,
              json::pointer_view const& path, json::value const& value)
{
    json::replace(v, path, value);
}

static void
apply_move(json::value& v, json::value const&,
           json::pointer_view const& src, json::pointer_view const& dest)
{
    json::move(v, src, v, dest, move_method::insert_or_replace);
}

static void
apply_copy(json::value& v, json::value const&,
           json::pointer_view const& src, json::pointer_view const& dest)
{
    auto const& src_v = json::find_or_throw(v, src);
    json::insert_or_replace(v, dest, src_v);
}

static void
apply_test(json::value const& v, json::value const& op,
           json::pointer_view const& path, json::value const& value)
{
    using E = patch::errc;

    auto const& target = json::find_or_throw(v, path);

    if (target != value) {
        throw patch::error {
            E::tested_value_does_not_match,
            op
        };
    }
}

static void
apply_patch(json::value& v, json::value const& op)
{
    assert(op.is_object());
    auto const& obj = op.get_object();

    json::pointer const path { obj.at("path").get_string() };

    auto const& type = obj.at("op");
    if (type == "add") {
        auto const& value = obj.at("value");

        apply_add(v, op, path, value);
    }
    else if (type == "remove") {
        apply_remove(v, path);
    }
    else if (type == "replace") {
        auto const& value = obj.at("value");

        apply_replace(v, path, value);
    }
    else if (type == "move") {
        json::pointer const src { obj.at("from").get_string() };

        apply_move(v, op, src, path);
    }
    else if (type == "copy") {
        json::pointer const src { obj.at("from").get_string() };

        apply_copy(v, op, src, path);
    }
    else if (type == "test") {
        auto const& value = obj.at("value");

        apply_test(v, op, path, value);
    }
    else {
        // unknow type will be ignored
    }
}

} // anonymous namespace

/*
 * class patch
 */
patch::
patch()
    : m_value(json::array())
{}

patch::
patch(std::string_view const s)
    : patch { json::parse(s), true }
{}

patch::
patch(json::value v, bool)
    : m_value(std::move(v))
{
    validate(m_value);
}

patch::
patch(json::value const& v1, json::value const& v2)
    : m_value(json::array())
{
    patch_generator gen { *this };

    diff(v1, v2, gen);
}

patch::const_iterator patch::
begin() const
{
    return m_value.get_array().begin();
}

patch::const_iterator patch::
end() const
{
    return m_value.get_array().end();
}

patch::size_type patch::
size() const
{
    return m_value.get_array().size();
}

patch& patch::
add(json::pointer_view const& path, json::value const& v)
{
    auto& arr = m_value.get_array();

    json::object obj;
    obj["op"] = "add";
    obj["path"] = path.to_value();
    obj["value"] = v;

    arr.push_back(std::move(obj));

    return *this;
}

patch& patch::
remove(json::pointer_view const& path)
{
    auto& arr = m_value.get_array();

    json::object obj;
    obj["op"] = "remove";
    obj["path"] = path.to_value();

    if (path.empty()) {
        throw patch::error {
            errc::empty_path_on_remove_operation,
            std::move(obj)
        };
    }

    arr.push_back(std::move(obj));

    return *this;
}

patch& patch::
replace(json::pointer_view const& path, json::value const& v)
{
    auto& arr = m_value.get_array();

    json::object obj;
    obj["op"] = "replace";
    obj["path"] = path.to_value();
    obj["value"] = v;

    arr.push_back(std::move(obj));

    return *this;
}

patch& patch::
move(json::pointer_view const& src, json::pointer_view const& dest)
{
    using E = patch::errc;

    auto& arr = m_value.get_array();

    if (rng::is_proper_prefix(src, dest)) {
        json::value v;
        auto& obj = v.emplace_object();
        obj["from"] = src.to_string();
        obj["path"] = dest.to_string();

        throw patch::error {
            E::from_location_can_not_be_proper_prefix_of_path_location,
            v
        };
    }

    json::object obj;
    obj["op"] = "move";
    obj["from"] = src.to_value();
    obj["path"] = dest.to_value();

    arr.push_back(std::move(obj));

    return *this;
}

patch& patch::
copy(json::pointer_view const& src, json::pointer_view const& dest)
{
    auto& arr = m_value.get_array();

    json::object obj;
    obj["op"] = "copy";
    obj["from"] = src.to_value();
    obj["path"] = dest.to_value();

    arr.push_back(std::move(obj));

    return *this;
}

patch& patch::
test(json::pointer_view const& path, json::value const& v)
{
    auto& arr = m_value.get_array();

    json::object obj;
    obj["op"] = "test";
    obj["path"] = path.to_value();
    obj["value"] = v;

    arr.push_back(std::move(obj));

    return *this;
}

json::value
apply_patch(json::value const& v, patch const& p)
{
    json::value result = v;

    for (auto const& op: p) {
        try {
            apply_patch(result, op);
        }
        catch (json::patch::error const&) {
            throw;
        }
        catch (json::pointer::error const&) {
            throw patch::error {
                patch::errc::pointer_error,
                op,
                std::current_exception()
            };
        }
        catch (...) {
            throw patch::error {
                patch::errc::internal_error,
                op,
                std::current_exception()
            };
        }
    }

    return result;
}

std::ostream&
operator<<(std::ostream& os, patch const& p)
{
    return os << p.m_value;
}

/*
 * class patch::error
 */
patch::error::
error(errc const e, json::value const& v)
    : std::runtime_error { str::to_string(e) }
    , m_ec { e }
    , m_value(v)
{}

patch::error::
error(errc const e, json::value const& v, std::exception_ptr const& nested)
    : std::runtime_error { str::to_string(e) }
    , m_ec { e }
    , m_value(v)
    , m_nested { nested }
{}

std::ostream&
operator<<(std::ostream& os, patch::errc const e)
{
    using E = patch::errc;

    switch (e) {
        case E::ok:
            os << "ok";
            break;
        case E::pointer_error:
            os << "pointer error";
            break;
        case E::internal_error:
            os << "pointer error";
            break;
        // patch semantic error
        case E::patch_document_has_to_be_array:
            os << "patch document has to be an array";
            break;
        case E::operation_is_not_object:
            os << "operation is not an object";
            break;
        case E::operation_does_not_contain_op_member:
            os << "operation does not contain op member";
            break;
        case E::op_member_has_to_be_string:
            os << "op member has to be a string";
            break;
        case E::operation_does_not_contain_path_member:
            os << "operation does not contain path member";
            break;
        case E::path_member_has_to_be_string:
            os << "path member has to be a string";
            break;
        case E::path_member_is_not_proper_json_pointer:
            os << "path member is not proper json pointer";
            break;
        case E::operation_does_not_contain_from_member:
            os << "operation does not contain from member";
            break;
        case E::from_member_has_to_be_string:
            os << "from member has to be a string";
            break;
        case E::from_member_is_not_proper_json_pointer:
            os << "from member is not proper json pointer";
            break;
        case E::operation_does_not_contain_value_member:
            os << "operation does not contain value member";
            break;
        case E::empty_path_on_remove_operation:
            os << "empty path on remove operation";
            break;
        case E::from_location_can_not_be_proper_prefix_of_path_location:
            os << "from location can not be proper prefix of path location";
            break;
        // operand semantic error
        case E::tested_value_does_not_match:
            os << "tested value does not match";
            break;
    }

    return os;
}

std::ostream&
operator<<(std::ostream& os, patch::error const& e)
{
    return os << json::value_from(e);
}

void
tag_invoke(json::value_from_tag, json::value& v, patch::error const& e)
{
    auto& obj = v.emplace_object();

    obj["type"] = "stream9::json::patch::error";
    obj["code"] = str::to_string(e.code());
    obj["value"] = e.value();

    try {
        e.rethrow_if_nested();
    }
    catch (json::pointer::error const& e) {
        obj["nested_error"] = json::value_from(e);
    }
    catch (std::exception const& e) {
        obj["nested_error"] = e.what();
    }
}

} // namespace stream9::json
